# pybs4 #

## Requires

minimarkup and pybs4 to be add to path


## Test sites ##

~~~
cd tests
python test_site.py
~~~

## Running tests ##

From the project route enter `pytest tests` to run all tests

To generate test coverage results for the util directory run the following command `pytest --cov=util  tests`
