from datetime import date, datetime, timedelta

from pybs4.datetime import add_one_month, adjust_ymd


def test_navigate_by_month_easy():
    # easy
    for day in range(27):
        assert add_one_month(f'201801{day+1:02d}', +1) == f'201802{day+1:02d}'
        assert add_one_month(f'201802{day+1:02d}', -1) == f'201801{day+1:02d}'


def test_navigate_by_month_last_day():
    assert add_one_month('20180131', +1) == '20180228'
    assert add_one_month('20180228', -1) == '20180131'
    assert add_one_month('20000131', +1) == '20000229'
    assert add_one_month('20000229', -1) == '20000131'


def test_leap(): # 2000
    # Reported from hypothesis
    assert add_one_month('20000126', +1) == '20000226'
    assert add_one_month('20000127', +1) == '20000227'
    assert add_one_month('20000128', +1) == '20000228'
    assert add_one_month('20000129', +1) == '20000229'
    assert add_one_month('20000130', +1) == '20000229'
    assert add_one_month('20000131', +1) == '20000229'

    assert add_one_month('20000226', -1) == '20000126'
    assert add_one_month('20000227', -1) == '20000127'
    assert add_one_month('20000228', -1) == '20000128'
    assert add_one_month('20000229', -1) == '20000131'

    assert add_one_month('20000331', -1) == '20000229'
    assert add_one_month('20000330', -1) == '20000229'
    assert add_one_month('20000329', -1) == '20000229'
    assert add_one_month('20000328', -1) == '20000228'
    assert add_one_month('20000327', -1) == '20000227'

    assert adjust_ymd('20000229', 'Year', +1) == '20010228'
    assert adjust_ymd('20010228', 'Year', -1) == '20000228'

def test_nonleap(): # 2001
    assert add_one_month('20010126', +1) == '20010226'
    assert add_one_month('20010127', +1) == '20010227'
    assert add_one_month('20010128', +1) == '20010228'
    assert add_one_month('20010129', +1) == '20010228'
    assert add_one_month('20010130', +1) == '20010228'
    assert add_one_month('20010131', +1) == '20010228'

    assert add_one_month('20010226', -1) == '20010126'
    assert add_one_month('20010227', -1) == '20010127'
    assert add_one_month('20010228', -1) == '20010131'

    assert add_one_month('20010331', -1) == '20010228'
    assert add_one_month('20010330', -1) == '20010228'
    assert add_one_month('20010329', -1) == '20010228'
    assert add_one_month('20010328', -1) == '20010228'
    assert add_one_month('20010327', -1) == '20010227'
