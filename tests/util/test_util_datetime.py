from hypothesis import given
from datetime import datetime, timedelta
import pybs4.util.datetime as swe_datetime
from hypothesis.strategies import datetimes, sampled_from, builds

swe_dt = datetimes(min_value=datetime(2000, 1, 1), max_value=datetime(2100, 12, 31))
swe_ymd = builds(swe_datetime.date2ymd, swe_dt)
swe_sign = sampled_from([1, -1])


@given(swe_dt)
def test_date2dmy(dt):
    date_str = swe_datetime.date2dmy(dt)
    assert len(date_str) == 10
    assert swe_datetime.dmy2date(date_str) == dt.date()


@given(swe_dt)
def test_date2ymd(dt):
    date_str = swe_datetime.date2ymd(dt)
    assert len(date_str) == 8
    assert swe_datetime.ymd2date(date_str) == dt.date()


@given(swe_dt)
def test_dmy2ymd(dt):
    dmy_str = swe_datetime.date2dmy(dt)
    ymd_str = swe_datetime.date2ymd(dt)
    assert ymd_str == swe_datetime.dmy2ymd(dmy_str)
    assert dmy_str == swe_datetime.ymd2dmy(ymd_str)


@given(swe_ymd, swe_ymd)
def test_diff_ymd(ymd1, ymd2):
    diff = swe_datetime.diff_ymd(ymd1, ymd2)
    assert isinstance(diff, timedelta)
    zero_td = timedelta(0)
    if ymd1 == ymd2:
        assert diff == zero_td
    elif ymd1 < ymd2:
        assert diff < zero_td
    else:
        assert diff > zero_td


@given(swe_ymd, swe_sign)
def test_add_day(ymd, days):
    new_ymd = swe_datetime.add_days(ymd, days)
    if days < 0:
        assert new_ymd < ymd
    else:
        assert ymd < new_ymd
