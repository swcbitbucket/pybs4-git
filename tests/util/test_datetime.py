from hypothesis import given
from datetime import datetime, timedelta
import pybs4.datetime as bs4_datetime
from hypothesis.strategies import datetimes, sampled_from, builds

bs4_dt = datetimes(min_value=datetime(2000, 1, 1), max_value=datetime(2100, 12, 31))
bs4_ymd = builds(bs4_datetime.date2ymd, bs4_dt)
bs4_option = sampled_from(bs4_datetime.OPTIONS)
bs4_sign = sampled_from([1, -1])


@given(bs4_dt)
def test_change_month(dt):
    ymd_str = bs4_datetime.date2ymd(dt)
    ymd_plus_one = bs4_datetime.change_month(ymd_str, 1)
    dt_plus_one = bs4_datetime.ymd2date(ymd_plus_one)
    assert dt_plus_one.day in range(28, 32)
    if dt.month == 12:
        assert dt_plus_one.month == 1
    else:
        assert dt_plus_one.month == dt.month + 1

    ymd_minus_one = bs4_datetime.change_month(ymd_str, -1)
    dt_minus_one = bs4_datetime.ymd2date(ymd_minus_one)
    assert dt_minus_one.day in range(28, 32)
    if dt.month == 1:
        assert dt_minus_one.month == 12
    else:
        assert dt_minus_one.month == dt.month - 1


@given(bs4_dt)
def test_add_one_month(dt):
    ymd_str = bs4_datetime.date2ymd(dt)
    ymd_plus_one = bs4_datetime.add_one_month(ymd_str, 1)
    dt_plus_one = bs4_datetime.ymd2date(ymd_plus_one)
    if dt.month == 12:
        assert dt_plus_one.month == 1
    else:
        assert dt_plus_one.month == dt.month + 1

    ymd_minus_one = bs4_datetime.add_one_month(ymd_str, -1)
    dt_minus_one = bs4_datetime.ymd2date(ymd_minus_one)
    if dt.month == 1:
        assert dt_minus_one.month == 12
    else:
        assert dt_minus_one.month == dt.month - 1


@given(bs4_ymd, bs4_option, bs4_sign)
def test_adjust_ymd(ymd, opt, sign):
    new_ymd = bs4_datetime.adjust_ymd(ymd, opt, sign)
    if opt != 'All':
        # Test you go somewhere and inverting get you back again (approx)
        assert new_ymd != ymd
        if 'This' not in opt:
            if  (ymd[4:6] == '02' or new_ymd[4:6] == '02'):
                # last 2 characters for Feb 2x>3x
                assert ymd[:-2] == bs4_datetime.adjust_ymd(new_ymd, opt, -1 * sign)[:-2]
            else: # last character for All other months 3x
                assert ymd[:-1] == bs4_datetime.adjust_ymd(new_ymd, opt, -1 * sign)[:-1]


@given(bs4_ymd, bs4_sign)
def test_add_day(ymd, days):
    new_ymd = bs4_datetime.add_days(ymd, days)
    if days < 0:
        assert new_ymd < ymd
    else:
        assert ymd < new_ymd


@given(bs4_ymd, bs4_option)
def test_min_ymd(ymd, opt):
    start_ymd = bs4_datetime.min_ymd(ymd, opt)
    assert start_ymd <= ymd
