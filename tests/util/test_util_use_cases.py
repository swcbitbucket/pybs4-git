from datetime import date, datetime, timedelta

from pybs4.util.datetime import date2dmy, dmy2date, date2ymd, ymd2date, ymdhms2datetime, datetime2ymdhms, today_ymd, now_ymdhms

def test_dmy():
    # Take a date
    dmy = date2dmy(date.today())
    assert date.today() == dmy2date(dmy)

    # Take a datetime
    dmy = date2dmy(datetime.now())
    assert date.today() == dmy2date(dmy)

    # Take a datetime date
    dmy = date2dmy(datetime.now().date())
    assert date.today() == dmy2date(dmy)

    # start with a date
    yesterday = date.today() - timedelta(days=1)
    assert dmy2date(date2dmy(yesterday)) == yesterday

    # start with a datetime
    yesterday = datetime.now() - timedelta(days=7)
    assert dmy2date(date2dmy(yesterday)) == yesterday.date()

def test_ymd():
    # As above but with dmy > ymd
    ymd = date2ymd(date.today())
    assert date.today() == ymd2date(ymd)

    ymd = date2ymd(datetime.now())
    assert date.today() == ymd2date(ymd)

    ymd = date2ymd(datetime.now().date())
    assert date.today() == ymd2date(ymd)

    yesterday = date.today() - timedelta(days=1)
    assert ymd2date(date2ymd(yesterday)) == yesterday

    yesterday = datetime.now() - timedelta(days=9)
    assert ymd2date(date2ymd(yesterday)) == yesterday.date()

def test_conversions():
    todatetime = ymdhms2datetime('20190131-121212')
    assert todatetime == datetime2ymdhms(todatetime)

def test_now():
    assert now_ymdhms() == '12/04/60 10:10:10'

def test_today():
    assert today_ymd() == '12/04/60'
