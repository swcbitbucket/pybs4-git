# Extend menu3.py with Option and action parameters
from pybs4.menu import Menu, Submenu, Action, Gosub, Dropdown, Header, Divider, Option
from pybs4.web import start

MENU = Menu(
    
    Submenu('Example-4',
        Action('Internal', 'internal.py'),
        Action('External', 'external.py'),
        Gosub('System'),
        Gosub('Test'),
        Dropdown('Help',
            Header('Info'),
            Action('About', 'about.py'),
            Divider(),
            Gosub('Documentation'),
            Gosub('Admin'), # new
        ),
    ),

    Submenu('System',
        Action('Performance', 'performance.py'),
        Action('Errors', 'errors.py'),
    ),

    Submenu('Test',
        Action('Status', 'test_status.py'),
        Action('Results', 'test_results.py'),
    ),

    Submenu('Documentation',
        Dropdown('Browse', # includes action paramaters
            Action('Browse Any', 'browse.py'),
            Action('Browse Python', 'browse.py', TYPE='.py'),
            Action('Browse Markdown', 'browse.py', TYPE='.md'),
        ),
        Action('Docs', 'docs.py'),
        Action('Credits', 'http://swevolution.co.uk/logo'),
    ),

    Submenu('Admin',
        Action('Users Edit', 'pybs4/generic_edit.py', FILE='users.tsv', COLS='Name\tEmail\tPassword'),
        Action('Roles Edit', 'pybs4/generic_edit.py', FILE='roles.tsv', COLS='User\tRole'),
    ),

    Option('Version', 
        'r0c1', 'r1c5', 'r2c3', 'All',
    ), # new
    Option('Build', 
        'Production', 'Deveopment', 'All',
    ), # new
)

if __name__ == '__main__':
    start(MENU)
