# Simple case: 1 menu examples of actions
from pybs4.menu import Menu, Submenu, Action
from pybs4.web import start

MENU = Menu(
    Submenu('Example-5',
        Action('Action-1', 'action1.py', role='SPECIAL'),
        Action('Action-2', 'action2.py'),
    role='ADMIN',
    ),
)

if __name__ == '__main__':
    start(MENU)
