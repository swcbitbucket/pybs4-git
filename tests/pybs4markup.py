"""
"""
import getpass

from minimarkup import local_style, leaflet, highcharts, template, chartjs, vis
from minimarkup import bs5 as bootstrap

user = getpass.getuser()
if 'steve' not in user:
    raise ModuleNotFoundError()

Q = local_style.Q

HIDE = 'display:none'

SCRIPT_RELOAD_DIV = Q.script('''
function reload_div(divid, url)
{
    $(divid).load(url)
}
    ''')


def reload_div(divid, ACTION, CALL, **kwd):
    """
    return an href suitable for reloading a div
    ACTION script name including .py
    CALL function in script
    kwd upper case KEY=val
    """
    result = [f"reload_div('#{divid}', '/load/{ACTION}?CALL={CALL}"]
    if kwd:
        result.append('&')
    bits = []
    for key in kwd:
        bits.append(f'{key.upper()}={kwd[key]}')
    result.append('&'.join(bits))
    result.append("');return false")
    return ''.join(result)


def click_toggle(name):
    """
    Name is a CSS class name.
    """
    return f"toggle_display('{name}')"


def toggle_buttons(label, name=None, btn='btn-primary', initial='-', **kwd):
    """
    Return a pair of toggle buttons that can toggle a class and each other.
    """
    if name is None:  # name will default to label
        name = label
    if initial == '-':  # +,- or -,+
        other = '+'
    else:
        initial, other = '+', '-'
    return Q.a(f'{initial}{label}', href='#', onclick=click_toggle(name), class_=f'btn {btn} {name}', **kwd) +\
        Q.a(f'{other}{label}', href='#', onclick=click_toggle(name), class_=f'btn {btn} {name}', style=HIDE, **kwd)


class Methods:
    """
    Local methods for Pages and Divs
    """

    def invariant_table(self, list_of_lists, header, table_classes='table-bordered'):
        """
        list_of_lists = list of lists
        header must be supplied as it may change
        return new list_of_lists and header
        """
        assert all(isinstance(elem, list) for elem in list_of_lists), 'Expected list of lists'
        # transpose / count + tabulate / transpose
        transpose = [list(i) for i in zip(*list_of_lists)]
        new_result = []
        new_header = []
        invariant_result = []
        for idx, col in enumerate(transpose):
            setcol = set(col)
            if len(setcol) != 1:
                new_result.append(col)
                new_header.append(header[idx])
            else:
                invariant_result.append([idx, header[idx], setcol.pop()])
        self.simple_table(invariant_result, header=['Column Index', 'Column Name', 'Constant Value'], table_classes=table_classes)
        new_result = [list(i) for i in zip(*new_result)]
        return new_result, new_header

    def bs_table(self, title, th_list, td_list, color='primary', data_height=1000, data_search='true', data_show_columns='true', data_pagination='true', data_sortable='true'):
        """
        Bootatrap Table inside a Bootstrap Card
        th_list is list of columns
        td_list is list of rows
        """
        len_cols = len(th_list)
        with self.divc(f'card border-{color}'):
            with self.divc(f'card-header text-bg-{color}'):
                self += title
            with self.divc('card-body'):
                with self.table(data_toggle='table', data_height=data_height, data_search=data_search, data_show_columns=data_show_columns, data_pagination=data_pagination):
                    with self.thead(class_=f'text-bg-{color}'):
                        with self.tr():
                            for col in th_list:
                                self.th(col, data_field=col.lower(), data_sortable=data_sortable)
                    with self.tbody():
                        for row in td_list:
                            assert len(row) == len_cols
                            with self.tr():
                                self.td(row)

    def simple_table(self, list_of_lists, header=None, row_callback=None, col_callback=None, table_classes='table-bordered', sort=False, highlight=False):
        """
        list_of_lists = list of lists
        optional header list of headers eg ['col1', 'col2']
        optional row_callback eg callback(row, last_row)
           return False for no row
           return None for no style and row as is
           return list for replacement row
           return dict for keywords for <tr> eg dict(style='color:green', class_='ident123')
           return tuple(list, dict)
        optional col_callback eg callback(row, idx, last_row)
           return False for no col
           return None for using col as is
           return modified col contents eg Q.a(row[idx], href='#') as str
           return dict for <td> keywords eg dict(style='color:green', class_='ident123')
           return tuple(str, dict)
        table_classes - style string for table eg 'lable-hover' etc
        sort allow table sort or not
        highlight changes use colour to see when columns change
        """
        # make sure we have list of lists
        assert all(isinstance(elem, (list, tuple)) for elem in list_of_lists), 'Expected list of lists'

        if highlight:
            assert row_callback is None and col_callback is None

            def col_callback(row, idx, last_row):
                """
                """
                if last_row is None or row[idx] != last_row[idx]:
                    return dict(style='background:teal;color:black')

        # default row callback
        if row_callback is None:
            def row_callback(row, last_row):
                pass

        # default col callback
        if col_callback is None:
            def col_callback(row, idx, last_row):
                pass

        with self.tablec(f'table {table_classes}'):

            # optional table header may be sorted
            if header is not None:
                if sort:
                    with self.thead():  # thead makes sortable
                        self.tr(Q.th(header))
                else:
                    self.tr(Q.th(header))

            last_row = None
            with self.tbody():  # Always have a table body
                for list_row in list_of_lists:

                    # Handle row by row
                    rowcb = row_callback(list_row, last_row)
                    if rowcb is False:  # ignore row
                        last_row = list_row
                        continue

                    row = list_row
                    rkwd = {}
                    if isinstance(rowcb, tuple):  # specify new row and rkwd
                        row, rkwd = rowcb
                    elif isinstance(rowcb, dict):  # specify new rkwd
                        rkwd = rowcb
                    elif isinstance(rowcb, list):
                        row = rowcb

                    with self.tr(**rkwd):  # The row

                        # Handle col by col
                        for idx in range(len(row)):
                            col = col_callback(row, idx, last_row)
                            if isinstance(col, tuple):
                                col, ckwd = col
                                assert isinstance(ckwd, dict)
                            elif isinstance(col, dict):
                                ckwd = col
                                col = None
                            else:
                                ckwd = {}
                            if col is False:  # allow col to be ommited
                                continue
                            if col is None:
                                col = row[idx]

                            self.td(col, **ckwd)  # The column
                    last_row = row


class Page(Methods, local_style.Methods, leaflet.Methods, chartjs.Methods, vis.Methods, highcharts.Methods, bootstrap.Methods, template.Page):
    """
    """

    def __init__(self, *a, **k):
        """
        # NOTE: no css for highcharts or chartsjs
        """
        super().__init__(*a,
                         csslist=[self.bs_css, self.vis_css_4, self.leaflet_css, self.local_css],
                         jslist=[self.bs_js, self.cjs_js_cdn, self.vis_js_4, self.leaflet_js, self.hc_js_7, self.local_js],
                         **k)


class Div(Methods, local_style.Methods, leaflet.Methods, chartjs.Methods, vis.Methods, highcharts.Methods, bootstrap.Methods, template.Div):
    """
    """
