# Simple case: 1 menu 2 actions
from pybs4.menu import Menu, Submenu, Action
from pybs4.web import start

MENU = Menu(
    Submenu('Example-1',
        Action('Internal', 'internal.py'),
        Action('External', 'external.py'),
    ),
)

if __name__ == '__main__':
    start(MENU)
