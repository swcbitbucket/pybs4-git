import sys
import datetime
import time

from pybs4.requests import Requests

URL = 'http://pbs4.swevolution.co.uk'
URL = 'http://localhost:8080'
AUTH = ('admin@admin', 'admin')

r = Requests(URL, AUTH)

def qprint(msg):
    if msg:
        print(f'\t{msg}')

qprint(r.json('dataview', 'POST', NAME='steve-data'))
qprint(r.json('dataview', 'PUT', NAME='steve-data', VALUE=23))
qprint(r.json('dataview', 'PUT', NAME='steve-data', DATE='12/04/2015', VALUE=25))
qprint(r.json('dataview', 'PUT', NAME='steve-data', TIME='09:12:00', VALUE=26))
qprint(r.json('dataview', 'PUT', NAME='steve-data', DATE='12/04/2015', TIME='09:24:59', VALUE=27))
qprint(r.json('dataview', 'GET', NAME='steve-data'))
qprint(r.json('dataview', 'DELETE', NAME='steve-data'))


qprint(r.json('dataview', 'DELETE', NAME='fred'))
qprint(r.json('dataview', 'POST', NAME='fred'))
count = 1
while count<200:
    dat, tim = str(datetime.datetime.now()).split()
    qprint(r.json('dataview', 'PUT', NAME='fred', DATE=dat, TIME=tim, VALUE=count, DEBUG=True))
    #time.sleep(2)
    count += 1
