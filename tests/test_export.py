import os
from pybs4.requests import Requests

URL = 'http://pbs4.swevolution.co.uk'
URL = 'http://localhost:8080'
AUTH = ('admin@admin', 'admin')

r = Requests(URL, AUTH)
print(r.text('export', 'files', FOLDER=f'{os.getcwd()}/pybs4'))
print(r.text('export', 'file', PATH=f'{os.getcwd()}/pybs4/requests.py'))
