# Simple case: 1 menu examples of actions
from pybs4.menu import Menu, Submenu, Action, Gosub
from pybs4.web import start

MENU = Menu(

    Submenu('Home',
        Gosub('Operational'),
        Gosub('Development'),
        Gosub('Help'),
        Gosub('Admin'),
        role=None,
        ),

    Submenu('Help',
        Action('Wiki', 'wiki.py'),
        role=None,
        ),

    Submenu('Operational',
        Action('Op Action-1', 'action1.py', ARG=1),
        Action('Op Action-2', 'action1.py', ARG=2),
        role='OPS',
        ),

    Submenu('Development',
        Action('Dev Action-2', 'action2.py', role='SPECIAL'),
        role='DEV',
        ),

    Submenu('Admin',
        Action('Users Edit', 'pybs4/generic_edit.py', FILE='users.tsv', COLS='Name\tEmail\tPassword'),
        Action('Roles Edit', 'pybs4/generic_edit.py', FILE='roles.tsv', COLS='Role'),
        Action('User Roles Edit', 'pybs4/generic_edit.py', FILE='user_roles.tsv', COLS='User\tRole'),
        Action('Check Integrity', 'pybs4/check_user_roles.py'),
        role='ADMIN',
        )
)

if __name__ == '__main__':
    start(MENU)
