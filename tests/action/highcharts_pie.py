from minimarkup.highcharts import DANGER, INFO, PRIMARY

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    with p.div(class_='row'):
        with p.div(class_='col-md-3'):
            p.hc_pie(PRIMARY, 'Env', ctxt.ENV, [dict(name='x', y=100), dict(name='y', y=200)])
        with p.div(class_='col-md-3'):
            p.hc_pie(INFO, 'Ver', ctxt.VER, [dict(color='blue', name='x', y=100), dict(color='green', name='y', y=200), dict(color='red', name='z', y=245)])
        with p.div(class_='col-md-3'):
            p.hc_pie(DANGER, 'Tim', ctxt.TIM, [dict(name='x', y=100), dict(name='y', y=100)])
