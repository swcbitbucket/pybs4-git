import os
import glob
import markdown

from pybs4.util.markup import Q

assert os.path.isdir('markdown')
INDEX = 'markdown/index.md'
assert os.path.exists(INDEX), INDEX


def menu(p, ctxt):
    with p.nav(class_='navbar navbar-fixed-top'):
        p.ctxt_submit('Index', CALL='content', MDFILE='index.md')
        with p.divc('ml-auto'):
            with p.ul(class_='navbar-nav'):
                with p.li():
                    p.ctxt_submit('A-Z', CALL='a2z')


def add_hrefs(str, ctxt):
    # [label mdfile.md] => a(label, href=mdfile)
    save = None
    result = []
    for c in str:
        if c == '[':
            assert save is None
            save = [c]
        elif c == ']':
            label, mdfile = ''.join(save[1:]).split()
            result.append(Q.p(Q.a(label, href=ctxt.href(MDFILE=mdfile))))
            save = None
        elif save:
            save.append(c)
        else:
            result.append(c)
    return ''.join(result)


def content(p, ctxt):
    mdfile = ctxt.get('MDFILE', 'index.md')
    mdfile = f'markdown/{mdfile}'
    exists = os.path.exists(mdfile)
    menu(p, ctxt)
    with p.divc('container'):
        with p.divc('jumbotron'):
            if exists:
                content = add_hrefs(open(mdfile).read(), ctxt)
                p += markdown.markdown(content, extensions=['markdown.extensions.extra'])
            else:
                p.h3('%s not found' % mdfile)


def a2z(p, ctxt):
    menu(p, ctxt)
    with p.divc('container'):
        with p.divc('jumbotron'):
            with p.table():
                for f in sorted(glob.glob('markdown/*.md')):
                    with p.tr():
                        with p.td():
                            mdfile = os.path.basename(f)
                            p.ctxt_link(mdfile, MDFILE=mdfile)


default = content
