import os
import datetime
import random

from jinja2 import Template

from pybs4.util.logging import debug
from pybs4.util.files import write, read

TFILE = 'dyntable.tsv'
TCOLS = 'DT\tVALUE'

if not os.path.exists(TFILE):
    write(TFILE, cols=TCOLS)


def delete(ctxt):
    """
    ctxt.DT is the row to be deleted
    """
    data = read(TFILE)
    write(TFILE, [r for r in data if r.DT != ctxt.DT])
    serve.refresh = True  # Trigger table refresh
    return dict()


def toggle(ctxt):
    """
    -1 is append to end 0 is prepend to front
    """
    serve.insertat = -1 if serve.insertat == 0 else 0
    serve.refresh = True # Trigger table refresh
    return dict()


def serve(ctxt):
    """
    ctxt.LAST is the last row received by the client.
    """
    data = read(TFILE)

    last = 0
    if data:
        last = data[-1].DT

    # First view of file
    if ctxt.LAST == 0:
        return dict(cmd='startup', arg=data, last=last, insertat=serve.insertat)

    # Check for refresh required
    if serve.refresh:
        serve.refresh = False
        return dict(cmd='refresh', arg=data, last=last, insertat=serve.insertat)

    # Updates only
    data = [d for d in data if d.DT>ctxt.LAST]
    return dict(cmd='update', arg=data, last=last, insertat=serve.insertat)
serve.refresh = False  # Set by delete to force a refresh
serve.insertat = 0  # Set by sort order to 0 or -1


def populate(p, ctxt):
    """
    Inject data into the table.
    """
    data = read(TFILE)
    if ctxt.pop('ADD', None):
        data.append((str(datetime.datetime.now()), str(len(data))))
        write(TFILE, data)
    with p.divc('row'):
        with p.divc('offset-4 col-2'):
            p.ctxt_submit(f'Add Data Point {len(data)}', CALL='populate', ADD='Yes')
        with p.divc('col-2'):
            p.ctxt_submit(f'View Table ', CALL='view')


def view(p, ctxt):
    """
    View the table and allow deletes.
    """
    tableid = p.divid
    # This script must be before the deletes of javascript can't find it
    p.script('''

        function toggle()
        // Insert or Append
        {
            $.ajax
            (
                {
                    url: '/json/dyntable',
                    data:
                    {
                        CALL: 'toggle'
                    },
                    cache: false
                }
            )
        }

        function deleterow(dt)
        // Delete a row
        {
            $.ajax
            (
                {
                    url: '/json/dyntable',
                    data:
                    {
                        CALL: 'delete',
                        DT: dt
                    },
                    cache: false
                }
            )
        }

        ''')
    with p.divc('row'):
        with p.divc('offset-5 col-1'):
            p.a(f'Toggle', href="#", onclick="toggle();return false")
        with p.divc('col-1'):
            p.p(None, id='toggleid')
    with p.divc('row'):
        with p.divc('offset-3 col-6'):
            with p.tablec('table-sm'):
                with p.thead():
                    with p.tr():
                        p.th('DT')
                        p.th('VALUE')
                        p.th('DELETE')
                with p.tfoot():
                    with p.tr():
                        p.th('DT')
                        p.th('VALUE')
                        p.th('DELETE')
                with p.tbody(id=tableid):
                    with p.tr():
                        pass # Add data here

    p.document_ready(Template('''

        last = 0 // nothing received at the start


        function addrows(table, data)
        // Add all new rows at the start or end
        {
            for (i=0; i < data.arg.length; i++)
            {
                row = table.insertRow(data.insertat)
                cell1 = row.insertCell(0)
                cell2 = row.insertCell(1)
                cell3 = row.insertCell(2)
                cell1.innerHTML = data.arg[i][0]
                cell2.innerHTML = data.arg[i][1]
                // You won't beleive how long it took me to get this line right!
                cell3.innerHTML = '<a href="#" onclick="deleterow(' + "'" + data.arg[i][0] + "'" + ');return false">Delete</a>'
            }
        }


        function request()
        // Call by timer to refresh table
        {
            $.ajax
            (
                {
                    url: '/json/dyntable',
                    data:
                    {   CALL: 'serve',
                        LAST: last
                    },
                    cache: false
                }
            ).done
            (
                function(data)
                {
                    // console.log(data)
                    last = data.last

                    toggle_el = document.getElementById("toggleid")
                    if (data.insertat == 0)
                    {
                        toggle_el.innerHTML = 'Insert'
                    }
                    else
                    {
                        toggle_el.innerHTML = 'Append'
                    }

                    table_el = document.getElementById("{{ tableid }}")
                    if (data.cmd == 'refresh')
                    {
                        // Only refresh needs rows deleted before adding
                        while (table_el.rows.length > 0)
                        {
                            table_el.deleteRow(0)
                        }
                    }
                    addrows(table_el, data)

                    setTimeout(request, {{ wait }})
                }
            )
        }

        setTimeout(request, {{ wait }})

        ''').render(wait=1000, tableid=tableid))
