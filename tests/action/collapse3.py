"""
"""
from random import choices, sample, randint

from pybs4.util.logging import error

from pybs4markup import Q, HIDE, toggle_buttons


def random_row(name, cols):
    """
    """
    return [name] + sample(range(12), cols)


def get_data(ctxt):
    """
    """
    data = []
    for idx in range(randint(10, 50)):
        for name, cols in choices([('short', 3), ('medium', 6), ('long', 9), ('slim', 1), ('empty', 0)]):
            data.append([idx] + random_row(f'extra-{name}', cols))  # store extra info for each col in the row before
            data.append([idx] + random_row(name, cols))
    return data


def row_callback(row, last_row):
    """
    """
    row_type = row[1]
    result = dict(class_=row_type)
    if row_type.startswith('extra-'):
        result['style'] = HIDE
    return result


def col_callback(row, idx, last_row):
    """
    """
    if idx == 0:  # hide the index column
        return False
    if row[1].startswith('extra-'):  # no format for extra data
        return None 
    if idx == 1:
        name = f'{row[0]}-{row[1]}'
        return toggle_buttons(label=row[1], name=name, btn='btn-warning btn-block', initial='+', title=f'Press to expand info on {name}') + Q.pre(str(last_row), class_=name, style=HIDE)
    return Q.a(row[idx], href='#', title=f'Extra {last_row[idx]}')


def default(p, ctxt):
    """
    Display a table with rcols that hold extra info.
    """
    data = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h1('Click Type to expand - hover over Data cells to see extra info')
            p.simple_table(data, header=['Type', 'Data...'], row_callback=row_callback, col_callback=col_callback)
