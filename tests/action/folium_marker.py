import folium


def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)
    m = folium.Map(
        location=[45.372, -121.6972],
        zoom_start=12,
        tiles='Stamen Terrain'
    )

    tooltip = 'Click me!'

    folium.Marker([45.3288, -121.6625], popup='<i>Mt. Hood Meadows</i>', tooltip=tooltip).add_to(m)
    folium.Marker([45.3311, -121.7113], popup='<b>Timberline Lodge</b>', tooltip=tooltip).add_to(m)

    p.leaflet_wrap_folium_map(m)
