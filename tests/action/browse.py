"""
"""

import os
import platform
import glob


def view(p, ctxt):
    """
    """
    if 'webfaction' in platform.node():
        return
    fname = ctxt.FILE
    ctxt.consume('FOLDER')
    p.ctxt_link('back', CALL='browse')
    p.h1(fname)
    p.pre(open(fname).read() or '--Empty--')


def browse(p, ctxt):
    """
    """
    if 'webfaction' in platform.node():
        return
    folder = ctxt.FOLDER or '.'
    ftype = ctxt.TYPE
    p.h1(f'{folder} {ftype}')
    with p.divc('col-2'):
        with p.ul():
            for fname in sorted(glob.glob(f'{folder}/*')):
                fname = fname.replace('./', '')
                if os.path.isdir(fname):
                    with p.li():
                        p.ctxt_link(fname, CALL='browse', FOLDER=fname)
                elif not ftype or fname.endswith(ftype):
                    with p.li():
                        if ftype:
                            p.ctxt_link(fname, CALL='view', FILE=fname)
                        else:  # just as an example
                            p.ctxt_link(fname, ACTION='viewfile.py', FILE=fname, BACK='browse.py')


default = browse
