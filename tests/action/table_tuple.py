"""
"""
from collections import namedtuple
from random import randint


def get_data(ctxt):
    """
    """
    Row = namedtuple('Row', ['Index', 'Type', 'Col1', 'Fixed', 'Col2', 'Last'])
    data = []
    for idx in range(randint(10, 50)):
        data.append(Row._make([idx, 'short', randint(0, 1), 'fixed', randint(0, 1), 'last']))
    return data


def default(p, ctxt):
    """
    Display a table with invariant columns and highlighted rows.
    """
    data = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h5('Table to Highlight columns that change')
            header = list(data[0]._fields)
            p.simple_table(data, header=header, highlight=True)
