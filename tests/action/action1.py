# Simple example given a div to work with (p) and a context (dict)
from pybs4.util.markup import Q

def default(p, ctxt):

    # Access to the variables will delete if set to False (see below)
    ctxt.ARG1
    ctxt.ARG2

    # Display a table with all values in the current context
    with p.divc('row'):
        with p.divc('col-4'):
            with p.tablec('tablesorter'):
                p.caption('Current Context')
                with p.thead():
                    p.tr(Q.th(['Key', 'Value']))
                with p.tbody():
                    for key in sorted(ctxt):
                        p.tr(Q.td([key, ctxt[key]]))

    # Create/ Delete and Modify context variables ARG1 and ARG2
    with p.divc('row'):
        with p.divc('col-2'):
            if ctxt.ARG1:
                val = int(ctxt.ARG1)
                p.ctxt_submit('Inc Arg1', ARG1=val+1)
                if val:
                    p.ctxt_submit('Set Arg1 = 0', ARG1=0)
                p.ctxt_submit('Delete Arg1', ARG1=False) # Causes delete
            else:
                p.ctxt_submit('Create Arg1 = 0', ARG1=0)

        with p.divc('col-2'):
            if ctxt.ARG2:
                val = int(ctxt.ARG2)
                p.ctxt_submit('Inc Arg2', ARG2=val+1)
                if val:
                    p.ctxt_submit('Set Arg2 = 0', ARG2=0)
                p.ctxt_submit('Delete Arg2', ARG2=False) # Causes delete
            else:
                p.ctxt_submit('Create Arg2 = 0', ARG2=0)
