import datetime
from minimarkup.chartjs import cjs_dt_list, F_BLUE, B_BLUE, F_RED, B_RED, F_YELLOW, B_YELLOW

from .testdata import minute_data

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)
    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_timeline(
            'MyTitle1',
            'MyYAxis1',
            datasets=dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel', fillcolor=F_BLUE, bordercolor=B_BLUE, fill='true', stepped='false'),
        )


    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_timeline(
            'MyTitle2',
            'MyYAxis2',
            datasets=[
                dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel1', fillcolor=F_BLUE, bordercolor=B_BLUE, fill='true', stepped='false'),
                dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel2', fillcolor=F_RED, bordercolor=B_RED, fill='true', stepped='true'),
            ]
        )

    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_timeline(
            'MyTitle3',
            'MyYAxis3',
            datasets=[
                dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel1', fillcolor=F_BLUE, bordercolor=B_BLUE, fill='true', stepped='false'),
                dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel2', fillcolor=F_RED, bordercolor=B_RED, fill='true', stepped='true'),
                dict(data=cjs_dt_list(minute_data(f'{ymd}-230000', mins=60, rmax=10, mingap=10)), label='MyLabel2', fillcolor=F_YELLOW, bordercolor=B_YELLOW, fill='true', stepped='false'),
            ],
            stacked=True,
        )
