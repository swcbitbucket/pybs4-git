"""
Test combinations of href to html
"""


def default(p, ctxt):
    """
    """
    with p.divc('col-3'):
        p.h1(ctxt.ACTION)

        p.h4('Raw anchor with pure href')
        p.a('raw anchor /static html', href='/static/Highcharts-7/index.htm')
        p.br()
        p.a('raw anchor static html', href='static/Highcharts-7/index.htm')
        p.br()
        p.a('raw anchor local html', href='idx.html')
        p.br()
        p.a('raw anchor relative html', href='html/relidx.htm')
        p.br()
        p.a('raw anchor http', href='http://swevolution.co.uk/logo')
        p.br()
        p.a('raw anchor https', href='https://webcards.swevolution.co.uk', target='_top')  # needs _top otherwise it breaks html rules
        p.hr()

        p.h4('ctxt.href with ACTION')
        p.a('ctxt href raw anchor /static', href=ctxt.href(ACTION='/static/Highcharts-7/index.htm'))
        p.br()
        p.a('ctxt href raw anchor static', href=ctxt.href(ACTION='static/Highcharts-7/index.htm'))
        p.br()
        p.a('ctxt href raw anchor local', href=ctxt.href(ACTION='idx.html'))
        p.br()
        p.a('ctxt href raw anchor relative', href=ctxt.href(ACTION='html/relidx.htm'))
        p.br()
        p.a('ctxt href http', href=ctxt.href(ACTION='http://swevolution.co.uk/logo'))
        p.br()
        p.a('ctxt href https', href=ctxt.href(ACTION='https://webcards.swevolution.co.uk'), target='_top')
        p.hr()

        p.h4('ctxt_link with ACTION')
        p.ctxt_link('ctxt_link /static html', ACTION='/static/Highcharts-7/index.htm')
        p.br()
        p.ctxt_link('ctxt_link static html', ACTION='static/Highcharts-7/index.htm')
        p.br()
        p.ctxt_link('ctxt_link local html', ACTION='idx.html')
        p.br()
        p.ctxt_link('ctxt_link relative html', ACTION='html/relidx.htm')
        p.br()
        p.ctxt_link('ctxt_link http:', ACTION='http://swevolution.co.uk/logo')
        p.br()
        p.ctxt_link('ctxt_link https:', ACTION='https://webcards.swevolution.co.uk')
        p.hr()
