def default(p, ctxt):

    p.ctxt_visualise()

    p.p('Navigation')
    p.ctxt_submit('Home', HOME=True)
    p.ctxt_submit('Clear', CLEAR=True)
    p.ctxt_submit('Gosub1', GOSUB='Sub1')
    p.ctxt_submit('Gosub2', GOSUB='Sub2')
    p.ctxt_submit('Gohome', GOSUB='Index')

    if ctxt.consume('ARG1'):
        p.ctxt_submit('Clear Arg1')
    else:
        p.ctxt_submit('Set Arg1', ARG1='Arg1')

    p.p('Change the menu selections')
    p.ctxt_submit('AllAllAll', ENV='All', VER='All', TIM='All')
