def badsignature():
    pass


def raisejsonexception(ctxt):
    raise Exception('from json')


def raiseloadexception(ctxt):
    raise Exception('from load')


def wrongjsonreturn(ctxt):
    return 'string'


def badjsondump(ctxt):
    return dict(result=set())


def inlineexception(p, ctxt):
    raise Exception('from inline')

