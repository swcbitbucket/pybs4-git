def frameexception(p, ctxt):
    raise Exception('frame exception raised')

def default(p, ctxt):
    with p.divc('col-3'):
        p.ctxt_visualise()
        p.h2('I have been given a page')
        p.h3(f'YMD {ctxt.YMD}')
        p.h3(f'ENV {ctxt.ENV}')
        if ctxt.ARG1:
            p.h3(ctxt.consume("ARG1"))
            p.ctxt_submit('Clear ARG1')
        else:
            p.ctxt_submit('Show ARG1', ARG1='ARG1')
