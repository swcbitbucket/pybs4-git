from pybs4.context import Page

def frameexception(ctxt):
    raise Exception('frame exception raised')


def default(ctxt):
    p = Page(ctxt)
    with p.divc('container-fluid hideonsubmit'):
        with p.divc('col-3'):
            p.ctxt_visualise()
            p.h1('My own page')
            p.h2(f'YMD {ctxt.YMD}')
            p.h2(f'ENV {ctxt.ENV}')
            if ctxt.ARG1:
                p.h2(ctxt.consume("ARG1"))
                p.ctxt_submit('Clear ARG1')
            else:
                p.ctxt_submit('Show ARG1', ARG1='ARG1')
    return p.render()
