from datetime import datetime
from pybs4.context import Page


def default(ctxt):
    """
    """
    p = Page(ctxt, 'CDN', hideonsubmit=True, refresh=ctxt.refresh(10))
    p.ctxt_visualise()
    p.h3(datetime.now())
    with p.div(class_='hideonsubmit'):
        p.h2(f'YMD {ctxt.YMD}')
        p.h2(f'ENV {ctxt.ENV}')
        if 'ARG1' in ctxt:
            p.h2(f'ARG1 {ctxt.ARG1}')

        p.ctxt_submit('Submit ARG1', ARG1='Help')
    return p.render()
