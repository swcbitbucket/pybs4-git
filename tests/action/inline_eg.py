from pybs4.util.markup import Div
from .inline_eg2 import default

def make(ctxt):
    p = Div(class_='hideonsubmit')
    default(p, ctxt)
    return p
