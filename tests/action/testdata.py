import datetime, random

from minimarkup.highcharts import DANGER, WARNING, INFO

import pybs4.util

def minute_data(start, mins=60*6, rmax=100, mingap=1):
    startdt = pybs4.util.datetime.ymdhms2datetime(start)
    data = {}
    for minutes in range(mins):
        data [str(startdt+ datetime.timedelta(minutes=mingap*minutes))]= max(0, random.gauss(rmax/2, rmax/6))
    return data

def minute_data2(start, mins=60*6, rmax=100, mingap=1):
    startdt = pybs4.util.datetime.ymdhms2datetime(start)
    data = {}
    for minutes in range(mins):
        data [str(startdt+ datetime.timedelta(minutes=mingap*minutes))]= sorted([max(0, random.gauss(rmax/2, rmax/6)),
                                                                          max(0, random.gauss(rmax/2, rmax/6))])
    return data

def onoff_data(start, mins=60*6, rmax=100, mingap=1):
    startdt = pybs4.util.datetime.ymdhms2datetime(start)
    data = {}
    for minutes in range(mins):
        this = 0 if random.randrange(rmax) % 11 else 1
        data [str(startdt+ datetime.timedelta(minutes=mingap*minutes))]= this
    return data

def color():
    return random.choice((DANGER, WARNING, INFO))
