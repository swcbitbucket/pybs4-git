from pybs4.util.markup import Div, Q

def default(p, ctxt):
    with p.divc('offset-3 col-6'):
        with p.tablec('table-hover table-sm'):
            with p.thead():
                p.tr(Q.th(['col1', 'col2', 'col3', 'col4']))
            with p.tbody():
                for i in range(10):
                    p.tr(Q.td([i, i*2, i*4, i**2]))
