"""
Simplest test
"""


def default(p, ctxt):
    """
    """
    with p.divc('row'):
        with p.table(data_toggle='table', data_height=400, data_search='true', data_show_columns='true'):
            with p.thead():
                with p.tr():
                    p.th('Key', data_field='key')
                    p.th('Value', data_field='value')
            with p.tbody():
                for idx in range(100):
                    with p.tr():
                        p.td(f'key{idx}')
                        p.td(f'val{idx}')

    with p.divc('row'):
        with p.divc('offset-1 col-10'):
            columns = ['Key', 'Value']
            rows = [[f'key{idx}', f'val{idx}'] for idx in range(2000)]
            p.bs_table('Test', columns, rows, data_height=400)
