import datetime

from minimarkup.highcharts import hc_bardata, DEFAULT, WARNING, DANGER, SUCCESS
from .testdata import minute_data

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)
    p.hc_timeseries_multi(DEFAULT, 'State Changes', 'state', [
        dict(color=SUCCESS, name='operational', 
            data={f'{ymd}-123000':1, f'{ymd}-124000':1, f'{ymd}-124001':0, f'{ymd}-124959':0, f'{ymd}-125000':1, f'{ymd}-134000':1}),
        dict(color=DANGER, name='failure', data={f'{ymd}-124000':1, f'{ymd}-125000':1}),
        dict(color=WARNING, name='warning', data={f'{ymd}-134000':1, f'{ymd}-145000':1}),
        ], max_=1.0)
    data1 = minute_data(f'{ymd}-190000')
    data2 = minute_data(f'{ymd}-190000', rmax=50)
    p.hc_timeseries_multi(DEFAULT, 'Random Bar', 'count',
        [dict(color=SUCCESS, name='good', data=hc_bardata(data1, datetime.timedelta(seconds=30))),
        dict(color=DANGER, name='danger', data=hc_bardata(data2, datetime.timedelta(seconds=30)))], max_=100)
    p.hc_timeseries_multi(DEFAULT, 'Random Raw', 'count',
        [dict(color=SUCCESS, name='good', data=data1),
        dict(color=WARNING,  name='bad', data=data2)], max_=100)
