import folium


def default(p, ctxt):
    """
    """
    ymd = ctxt.YMD
    p.h1(ymd)

    m = folium.Map(location=[51, -1])
    p.leaflet_wrap_folium_map(m)
