from minimarkup.highcharts import DEFAULT

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)
    p.hc_areaspline(DEFAULT, ctxt.ENV, 'ytitle',
        ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
        [dict(pointPlacement=0.0, name='s1', data=[70, 100, 150, 40, 45, 60, 30, 40]),
        dict(pointPlacement=0.0, name='s2', data=[80, 10, 15, 40, 60, 30, 40, 45]),
        dict(pointPlacement=0.0, name='s3', data=[10, 15, 40, 60, 30, 40, 45, 33]),
        ])
