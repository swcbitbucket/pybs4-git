def process(p, ctxt):
    with p.divc('row'):
        with p.divc('col-4'):
            p.ctxt_visualise()
            p.h1(f'Selected {ctxt.consume("SELECTED")}')
            p.ctxt_submit('Back', CALL='select')

def select(p, ctxt):
    p.ctxt_visualise()

    with p.form(class_='form-inline', method='post', action='/action'):
        p.ctxt_hidden(CALL='process')
        with p.select(class_='form-control', name='SELECTED'):
            p.option('One', value=1)
            p.option('Two', value=2)
            p.option('Three', value=3)
        p.button('Submit', class_='btn btn-primary', type='submit')

default = select
