from .testdata import minute_data, minute_data2


def default(p, ctxt):

    p.hc_area_range_and_line(
        title = 'Title',
        suffix = 'Suffix',
        rangesname = 'Range',
        ranges = minute_data2('20190101-000000'),
        averagesname = 'Averages',
        averages = minute_data('20190101-000000'),
        )
