"""
"""

from random import choices, sample, randint

from pybs4markup import Q, toggle_buttons


def random_row(name, cols):
    """
    """
    return [name] + sample(range(12), cols)


def get_data(ctxt):
    """
    """
    data = []
    extra = []
    types = set()
    for idx in range(randint(10, 50)):
        for name, cols in choices([('short', 3), ('medium', 6), ('long', 9), ('slim', 1), ('empty', 0)]):
            row = random_row(name, cols)
            data.append([idx] + row)
            extra.append(f'Extra modal data about Index {idx} {row}')
            types.add(row[0])
    return sorted(types), data, extra


def table_row_callback(row, last_row):
    """
    """
    row_type = row[1]
    result = dict(class_=row_type)  # tag each row so it can be show/hide
    return result


def table_col_callback(row, col_idx, last_row):
    """
    """
    if col_idx == 0:  # hide the index column
        return False
    row_idx = row[0]
    row_type = row[1]
    col = row[col_idx]
    if col_idx == 1:  # Modal Popup
        return Q.button(col, type_='button', class_='btn btn-primary btn-block', data_toggle='modal', data_target=f'#modal-{row_idx}')
    if col_idx == 2:  # Collapse extra content in cell
        ident = f'collapse-{row_idx}-{col_idx}'
        return Q.a(col, data_toggle='collapse', href=f'#{ident}', role='button') + Q.div(Q.div(f'Extra column data {row_idx} {col_idx} {row_type}', class_='card card-body'), class_='collapse', id=ident)
    return col


def select_col_callback(row, idx, last_row):
    """
    """
    return toggle_buttons(row[idx])


def default(p, ctxt):
    """
    Display a table with filter/modal and collapse.
    """
    types, data, extra = get_data(ctxt)

    # Select by Type
    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h5('Select visible Types/click Type for modal popup/Click Data for extra col data')
            p.simple_table([types], header=['Select'], col_callback=select_col_callback)

    # The main Table
    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.simple_table(data, header=['Type', 'Data...'], row_callback=table_row_callback, col_callback=table_col_callback)

    # The extra modal data
    for row_idx, edata in enumerate(extra):
        with p.divc('modal', id=f'modal-{row_idx}', tabindex='-1', role='dialog'):
            with p.divc('modal-dialog'):
                with p.divc('modal-content'):
                    with p.divc('modal-header'):
                        p.h5(f'Index {row_idx}')
                        p.button('&times', type_='button', class_='close', data_dismiss='modal')
                    with p.divc('modal-body'):
                        with p.pre():
                            p += edata
