def default(p, ctxt):
    with p.divc('col-4'):
        p.alert('local table')
        p.local_table([[1, 2, 3], [4, 5, 6], [7, 8, 9]], header=['One', 'Two', 'Three'])
