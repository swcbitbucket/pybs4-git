import os
import datetime
import time

from pybs4.util.markup import Q
from pybs4.util.logging import debug

import json

_unique = 1


def strip(result):
    """
    keys: _x_key will be sorted and key will be output bare
    '__string__' will be output bare
    """
    result = json.dumps(result, separators=(',',':'), sort_keys=True, indent=3)
    for order in '0123456789':
        result=result.replace('"_%s_'%order,'')
    return result.replace('"__','').replace('":',':').replace('__"','').replace('"',"'")


def expand(ident, params):
    return """
%s = new Highcharts.Chart(%s)
""" % (ident, strip(params))


def chartident():
    global _unique
    ident = 'chart_id%s' % _unique
    _unique += 1
    return ident


def datetimescript(ident, title, data, load=None):
    chart=dict(
            _1_renderTo=ident,
            _2_type='area',
            _3_zoomType='x')
    if load:
        chart['_4_events']=dict(_1_load='__%s__' % load)
    result = dict(
        _1_chart=chart,
        _2_title=dict(
            _1_text=title),
        _3_xAxis=dict(
            _1_type='datetime'),
        _4_plotOptions=dict(
            _1_area=dict(
                _1_fillColor=dict(
                    _1_linearGradient=dict(
                        _1_x1=0,
                        _2_y1=0,
                        _3_x2=1,
                        _4_y2=1),
                    _2_stops=[[0,'#7cb5ec'],[1,'white']]))),
        _5_series=[dict(
            _1_name='%s data' % title,
            _2_data='__%s__' % data)])
    return expand (ident, result)


class Model:
    VERSION = 'r0c0'
    ROOT = os.path.abspath('.')
    PATH=f'{ROOT}/streams{VERSION}'
    ESIZE = 34
    MAXE = 60*24
    MAXE2 = MAXE//2

    def __init__(self):
        if not os.path.exists(self.PATH):
            os.mkdir(self.PATH)

    def datetime(self, d,t):

        try: # 12/04/2015 12:23:27
            return datetime.datetime.strptime(f'{d} {t.split(".")[0]}', '%d/%m/%Y %H:%M:%S')
        except Exception as msg:
            pass

        try: # 2018-12-18 19:51:31.682373
            return datetime.datetime.strptime(f'{d} {t.split(".")[0]}', '%Y-%m-%d %H:%M:%S')
        except Exception as msg:
            debug(msg)
            return f'invalid date time ({d}, {t}) {msg}'

    def unixsecs(self, d, t):
        return int(time.mktime(self.datetime(d, t).utctimetuple()))

    def highcharts(self, data):
        result = []
        for d, t, v in sorted(data):
            result.append([1000*self.unixsecs(d, t), float(v)]) # highcharts needs miliseconds not seconds
        return result

    def value(self, v):
        try:
            return float(v)
        except Exception as msg:
            return 'invalid value (%s)' % v

    def stream(self, name, exists):
        if not name:
            return 'Missing Stream'
        if not name.replace('-','').isalnum():
            return 'Stream name can only contain dash (-), letters and digits'
        path = f'{self.PATH}/{name}'
        if not exists and os.path.exists(path):
            return f'Stream name {name} already exists'
        if exists and not os.path.exists(path):
            return f'Stream name {name} not found'
        return path # valid if startswith self.PATH

    def streamexists(self, name):
        result = self.stream(name, True)
        #debug(f'streamexists? {name} {result}')
        return True if result.startswith(self.PATH) else result

    def create(self, name):
        path = self.stream(name, exists=False)
        debug('create %s ' % path)
        if not path.startswith(self.PATH):
            return path
        open(path, 'w').close()

    def delete(self, name):
        path = self.stream(name, exists=True)
        debug('delete %s' % path)
        if not path.startswith(self.PATH):
            return path
        os.remove(path)

    def put(self, name, date, time, value):
        path = self.stream(name, exists=True)
        #debug('put %s' % path)
        if not path.startswith(self.PATH):
            return path
        dt = self.datetime(date, time)
        if not isinstance(dt, datetime.datetime):
            return dt
        v = self.value(value)
        if not isinstance(v, float):
            return v
        entries = os.stat(path).st_size//self.ESIZE
        if entries>=self.MAXE:
            debug('purge %s' % path)
            lines = open(path).read()
            open(path,'w').write(lines[self.ESIZE*self.MAXE2:])
        e = '%s\t%+e\n' % (dt, v)
        assert len(e)==self.ESIZE, e
        open(path, 'a').write(e)

    def get(self, name):
        path = self.stream(name, exists=True)
        #debug('get %s' % path)
        if not path.startswith(self.PATH):
            return path
        return [_.split() for _ in open(path).readlines()]


def bad(error=None):
    result = dict(status=500)
    if error is not None:
        result['error'] = error
    return result


def PUT(ctxt):
    name = ctxt.NAME
    _date, _time = str(datetime.datetime.now()).split()
    date = ctxt.pop('DATE', _date)
    time = ctxt.pop('TIME', _time)
    value = ctxt.consume('VALUE')
    error = Model().put(name, date, time, value)
    if error:
        return bad(error)
    return {}


def POST(ctxt):
    name = ctxt.NAME
    if name is None:
         return bad()
    error = Model().create(name)
    if error:
         return bad(error)
    return {}


def DELETE(ctxt):
    name = ctxt.consume('NAME')
    if name is None:
         return bad()
    error = Model().delete(name)
    if error:
         return bad(error)
    return {}


def GET(ctxt):
    name = ctxt.NAME
    if name is None:
         return bad()
    result = Model().get(name)
    if isinstance(result, str):
        return bad(result)
    return dict(name=name, result=result)


def delete(p, ctxt):
    p.h3(f'DataView Delete {ctxt.NAME}')
    p.ctxt_visualise()
    ctxt['ERROR']  = DELETE(ctxt)
    index(p, ctxt)


def inline_view(p, ctxt):
    p.h3(f'DataView Inline View {ctxt.NAME}')
    p.ctxt_visualise()
    result = GET(ctxt)
    assert isinstance(result, dict)
    if 'result' in result:
        ctxt['RESULT'] = result
    else:
        ctxt['ERROR'] = result
    index(p, ctxt)


def add_now(p, ctxt): # auto add date and time
    p.h3(f'DataView Add Now {ctxt.NAME}')
    #p.ctxt_visualise()
    ctxt['DATE'], ctxt['TIME'] = str(datetime.datetime.now()).split()
    ctxt['ERROR'] = PUT(ctxt)
    index(p, ctxt)


def add(p, ctxt): # add selected value
    p.h3(f'DataView Add {ctxt.NAME}')
    p.ctxt_visualise()
    ctxt['ERROR'] = PUT(ctxt)
    index(p, ctxt)


def new(p, ctxt):
    p.h3(f'DataView New {ctxt.NAME}')
    p.ctxt_visualise()
    ctxt['ERROR'] = POST(ctxt)
    index(p, ctxt)


def update(p, ctxt):
    p.h3('DataView Update')
    #p.ctxt_visualise()
    exists = Model().streamexists(ctxt.NAME)
    if exists is not True:
        return bad()
    with p.divc('row'):
        with p.divc('col-md-8 offset-md-2'):
            if ctxt.VALUE not in (None, True):
                val = ctxt.VALUE
                dt = str(datetime.datetime.now()).split()
                ctxt['DATE'], ctxt['TIME'] = dt
                error = PUT(ctxt)
                if isinstance(error, dict) and 'error' in error:
                    p.alert(error, 'danger')
                else:
                    p.alert(f'Set {val} @ {dt}')
            with p.tablec():
                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='update')
                    p.tr(Q.th('Add a value (@Today&Now)...'))
                    p.tr(Q.td(['Data Stream', ctxt.NAME]))
                    p.tr(Q.td(['Value', Q.input(type_='text', name='VALUE', autocomplete='off'), 'integer/float']))
                    p.tr(Q.td(['', Q.input(type='submit')]))
            p.ctxt_submit('Index', CALL='index')
    footer(p)


def dtchart(p, title, data):
    ident = chartident()
    p.div(id=ident, style='width:100%;height:400px').close()
    p.javascript_inline(datetimescript(ident, title, data))


def chart(p, result):
    if not result:
        return
    dtchart(p, result['name'], Model().highcharts(result['result']))


def footer(p):
    p.p(Q.small('WARNING: data on this site may be deleted at any time'))
    p.a(Q.small('SW Evolution Limited &copy; 2018'), href='http://swevolution.co.uk/logo')


def quick_view(p, ctxt):
    p.h3('DataView Quick View')
    p.ctxt_visualise()
    exists = Model().streamexists(ctxt.NAME)
    if exists is not True:
        return bad()
    result = GET(ctxt)
    chart(p, result)
    p.ctxt_submit('Index', CALL='index')
    footer(p)


def live(p, ctxt):
    p.h3('DataView Live View')
    #p.ctxt_visualise()
    exists = Model().streamexists(ctxt.NAME)
    if exists is not True:
        return bad()
    result = GET(ctxt)
    ident = chartident()
    p.div(id=ident, style='width:100%;height:400px').close()
    footer(p)
    data = Model().highcharts(result['result'])
    result['lastt'] = data[-1][0]
    result['ident'] = ident
    callback = """
var %(ident)s;
var lastt_%(ident)s=%(lastt)s;

function request_%(ident)s()
{
    $.ajax({
        url: '/json/dataview',
        data: {CALL: 'ajaxget', NAME: '%(name)s', LASTTIME: lastt_%(ident)s},
        cache:false}).done (
    function ( data )
    {
        thist = data.result[0];
        if (thist!==0)
        {
            %(ident)s.series[0].addPoint(data.result);
            lastt_%(ident)s = thist;
            setTimeout(request_%(ident)s, 500);
        }
        else
        {
            setTimeout(request_%(ident)s, 20*1000);
        }
    }).fail(
    function()
    {
        setTimeout(request_%(ident)s, 20*1000)
    })
}

setTimeout(request_%(ident)s, 20*1000)
""" % result

    p.javascript_inline("""
$(document).ready(function() {
%s
%s
})
""" % (callback, datetimescript(ident, result['name'], data)))

def index(p, ctxt):
    p.h3('DataView Index')
    #p.ctxt_visualise()

    name = ctxt.consume('NAME') or ''
    error = ctxt.consume('ERROR')
    result = ctxt.consume('RESULT')

    with p.divc('row'):
        with p.divc('col-md-8 offset-md-2'):
            if isinstance(error, dict) and 'error' in error:
                p.alert(error, 'danger')
            with p.tablec('table-sm'):

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='new')
                    p.tr(Q.th('Make a new...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', autocomplete='off'), 'Must not exist']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden()
                    p.tr(Q.th('Select a...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', autocomplete='off'), 'Must exist']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='add_now')
                    p.tr(Q.th('Add a value (@Today&Now)...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', value=name, autocomplete='off'), 'Must Exist']))
                    p.tr(Q.td(['Value', Q.input(type_='text', name='VALUE', autocomplete='off'), 'integer/float']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='add')
                    p.tr(Q.th('Add a value (@Date&Time)...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', value=name, autocomplete='off'), 'Must Exist']))
                    _date, _time = str(datetime.datetime.now()).split()
                    p.tr(Q.td(['Date', Q.input(type_='text', name='DATE', value=_date), 'yyyy-mm-dd']))
                    p.tr(Q.td(['Time', Q.input(type_='text', name='TIME', value=_time.split('.')[0]), 'hh:mm:ss']))
                    p.tr(Q.td(['Value', Q.input(type_='text', name='VALUE', autocomplete='off'), 'integer/float']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='inline_view')
                    p.tr(Q.th('View a...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', value=name, autocomplete='off'), 'Must Exist']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

                with p.form(action='/action', method='POST'):
                    p.ctxt_hidden(CALL='delete')
                    p.tr(Q.th('Delete a...'))
                    p.tr(Q.td(['Data Stream', Q.input(type_='text', name='NAME', value=name, autocomplete='off'), 'Must Exist']))
                    p.tr(Q.td(['', Q.input(type='submit')]))

    chart(p, result)

    if name:
        with p.divc('row'):
             with p.divc('col-md-2 offset-md-5'):
                with p.tablec():
                    p.tr(Q.th('%s Bookmarks' % name))
                    p.tr(Q.td(Q.a('Quick View', href=ctxt.href(CALL='quick_view', NAME=name))))
                    p.tr(Q.td(Q.a('Live View', href=ctxt.href(CALL='live',NAME=name))))
                    p.tr(Q.td(Q.a('Update', href=ctxt.href(CALL='update', NAME=name))))

    footer(p)


def ajaxget(ctxt):
    #debug(f'Ajax {ctxt}')
    unixsecs = Model().unixsecs
    lasttime = int(ctxt.LASTTIME)
    result = GET(ctxt)
    #debug(f'Ajax GET result {result}')
    for d, t, v in sorted(result['result']):
        thistime = 1000*unixsecs(d, t)
        if thistime>lasttime:
            #debug(f'New value {v} @ {d} {t}')
            return dict(result=[thistime, float(v)])
    return dict(result=[0,0])


default = index
