"""
"""
from pybs4markup import Q, Div, reload_div, SCRIPT_RELOAD_DIV


def show_or_hide_view(ctxt):
    """
    """
    p = Div(id=ctxt.DIVID)
    if ctxt.CMD != 'hide':
        with p.divc('row'):
            with p.divc('offset-2 col-8'):
                p.h4(f'{ctxt.CMD} {ctxt.IDX}')
                p.simple_table([[1,2], [3,4], [5,6]])
    return p.render()


def show_view(ctxt):
    """
    """
    p = Div(id=ctxt.DIVID)
    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h4(f'Row {ctxt.ROWIDX} Col {ctxt.COLIDX}')
            p.simple_table([[1,2], [3,4], [5,6]])
    return p.render()


def hide_view(ctxt):
    """
    """
    p = Div(id=ctxt.DIVID)
    return p.render()


def reload_show_hide(what, ACTION, CALL, DIVID, **kwd):
    """
    """
    return Q.a('View', href='#', onclick=reload_div(DIVID, ACTION=ACTION, CALL=CALL, CMD='view', DIVID=DIVID, **kwd))+'/'+\
           Q.a('Hide', href='#', onclick=reload_div(DIVID, ACTION=ACTION, CALL=CALL, CMD='hide', DIVID=DIVID, **kwd))+' '+what


def reload_view(name, ACTION, CALL, DIVID, **kwd):
    """
    """
    return Q.a(name, href='#', onclick=reload_div(DIVID, ACTION=ACTION, CALL=CALL, DIVID=DIVID, **kwd))


def default(p, ctxt):
    """
    """
    p += SCRIPT_RELOAD_DIV

    def col_callback1(row, idx, last_row):
        """
        """
        rowidx = row[0]
        divid = f'divid1{rowidx}'
        if idx == 1:
            return reload_show_hide('Extra', ACTION='reload_click.py', CALL='show_or_hide_view', DIVID=divid, IDX=rowidx)
        if idx == 2:
            return dict(id=divid)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.simple_table([[1, None, None], [2, None, None], [3, None, None]], header=['Index', 'Show/Hide', 'Extra'], col_callback=col_callback1, table_classes='table-bordered table-sm')

    def col_callback2(row, idx, last_row):
        """
        """
        if idx == 0:
            return None
        rowidx = row[0]
        divid = f'divid2{rowidx}'
        if idx in (1, 2):
            return reload_view(f'Show Extra Row {rowidx} Col {idx}', ACTION='reload_click.py', CALL='show_view', DIVID=divid, ROWIDX=rowidx, COLIDX=idx)
        if idx == 3:
            return reload_view(f'Hide Extra Row {rowidx}', ACTION='reload_click.py', CALL='hide_view', DIVID=divid)
        return dict(id=divid)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.simple_table([[1, None, None, None, None], [2, None, None, None, None], [3, None, None, None, None]], 
            header=['Index', 'Col1', 'Col2', 'Hide', 'Extra'], col_callback=col_callback2, table_classes='table-bordered table-sm')

