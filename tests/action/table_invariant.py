"""
"""
from random import choices, sample, randint


def get_data(ctxt):
    """
    """
    header = ['Index', 'Type', 'Col1', 'Fixed', 'Col2', 'Last']
    data = []
    for idx in range(randint(10, 50)):
        data.append([idx, 'short', randint(0, 1), 'fixed', randint(0, 1), 'last'])
    return data, header


def default(p, ctxt):
    """
    Display a table with invariant columns and highlighted rows.
    """
    data, header = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-4'):
            p.h5('Table of Invariant columns')
            data, header = p.invariant_table(data, header=header, table_classes='table-sm')

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h5('Table to Highlight columns that change')
            p.simple_table(data, header=header, highlight=True)
