def default(p, ctxt):
    p.ctxt_link('back', ACTION=ctxt.consume('BACK'))
    fname = ctxt.pop('FILE')
    p.h2(fname)
    p.pre(open(fname).read() or '--Empty--')
