from minimarkup.highcharts import INFO

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    p.hc_column(INFO, '1st Quarter', 'Count',
        ['Jan', 'Feb', 'Mar', 'Apr'],
        [dict(name='First',data=[100, 40, 60, 40]),
        dict(name= 'Next',data=[100, 48, 69, 49]),
        dict(name='Second',data=[200,90,30,40])])
