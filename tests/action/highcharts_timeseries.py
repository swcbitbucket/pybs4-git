import datetime

from minimarkup.highcharts import hc_bardata, DEFAULT, WARNING, DANGER, PRIMARY
from .testdata import minute_data


def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    with p.divc('row'):
        with p.divc('offset-1 col-10'):
            p.hc_timeseries_single(DEFAULT, 'Nice',
                                   dict(color=WARNING, name='operational', data={f'{ymd}-124000': 21, f'{ymd}-125001': 10, f'{ymd}-125959': 30, f'{ymd}-131000': 31, f'{ymd}-132000': 0, f'{ymd}-134000': 21}), max_=100)
    with p.divc('row'):
        with p.divc('offset-1 col-10'):
            p.hc_timeseries_single(DEFAULT, 'Bar',
                                   dict(color=DANGER, name='count', data=hc_bardata({f'{ymd}-124000': 21, f'{ymd}-125001': 10, f'{ymd}-125959': 30, f'{ymd}-131000': 31, f'{ymd}-132000': 0, f'{ymd}-134000': 21})), max_=100)
    with p.divc('row'):
        with p.divc('offset-1 col-10'):
            p.hc_timeseries_single(DEFAULT, 'Random Bar',
                                   dict(color=PRIMARY, name='count', data=hc_bardata(minute_data(f'{ymd}-190000'), datetime.timedelta(seconds=30))), max_=100)
    with p.divc('row'):
        with p.divc('offset-1 col-10'):
            p.hc_timeseries_single(DEFAULT, 'Random Raw', dict(color=PRIMARY, name='count', data=minute_data(f'{ymd}-190000')), max_=100)
