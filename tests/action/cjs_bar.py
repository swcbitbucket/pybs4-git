from minimarkup.chartjs import F_BLUE, B_BLUE, F_RED, B_RED, F_YELLOW, B_YELLOW
from .testdata import minute_data

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_bar('1st Quarter', 'Count',
            ['Jan', 'Feb', 'Mar', 'Apr'],
            [dict(label='myLabel1', data=[1,2,3,4], fillcolor=F_BLUE, bordercolor=B_BLUE),
            dict(label='myLabel2', data=[2,1,4,3], fillcolor=F_RED, bordercolor=B_RED),
            dict(label='myLabel3', data=[4,5,1,-1], fillcolor=F_YELLOW, bordercolor=B_YELLOW)])
    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_bar('Months', 'Count',
            ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June'],
            dict(label='myLabel1', data=[1, 2, 3, 4, 5, 6], fillcolor=F_BLUE, bordercolor=B_BLUE),
            )