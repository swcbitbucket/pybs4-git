import random

from minimarkup.highcharts import hc_time, hc_value
from pybs4.util.datetime import now_ymdhms


def live001(ctxt):
    return dict(result=[hc_time(now_ymdhms()), hc_value(random.randrange(100))])


def view(p, ctxt):
    for row in range(3):
        with p.divc('row'):
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}01', refresh=7, title=f'Chart {row} 1')
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}02', refresh=11, title=f'Chart {row} 2')
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}03', refresh=13, title=f'Chart {row} 3')


default = view
