def default(p, ctxt):
    p.ctxt_visualise()
    for m in ('test_action_url?MSG=Hello World', 'wiki', 'options.py', 'browse', 'browse?TYPE=py', 'viewfile.py?FILE=pybs4/menu.py', 'browse?TYPE=.py&CALL=view&FILE=site.py&FOLDER=.',):
        p.a(m, href=f'/action/{m}')

