"""
"""
from random import choices, sample, randint


def random_row(name, cols):
    """
    """
    return [name] + sample(range(2), cols)


def get_data(ctxt):
    """
    """
    data = []
    for idx in range(randint(10, 50)):
        for name, cols in choices([('short', 2)]):
            data.append([idx] + random_row(name, cols))
    return data


def default(p, ctxt):
    """
    Display a table with highlighted rows.
    """
    data = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h5('Table to Highlight columns that change')
            p.simple_table(data, header=['Index', 'Type', 'Col1', 'Col2'], highlight=True)
