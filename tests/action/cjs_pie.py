from minimarkup.chartjs import F_BLUE, F_RED, F_YELLOW, F_PURPLE, F_ORANGE

def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_pie(
            'Pie1',
            [
                'Jan',
                'Feb',
                'Mar'
            ],
            [
                F_BLUE,
                F_RED,
                F_YELLOW
            ],
            [
                dict(data=[1, 2, 3]),
                dict(data=[2, 1, 4]),
                dict(data=[4, 5, 1])
            ]
        )
    with p.divc('row', style=f'position: relative; height:400px; width:100%'):
        p.cjs_pie(
            'Pie2',
            [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
            ],
            [
                F_BLUE,
                F_RED,
                F_YELLOW,
                F_PURPLE,
                F_ORANGE,
            ],
            dict(data=[1, 2, 3, 4, 5]),
            cutout=50,
        )
