import random

from pybs4.util.markup import Div, Q
from pybs4.util.datetime import now_ymdhms

def live001(ctxt):
    return dict(result=[hc_time(now_ymdhms()), hc_value(random.randrange(100))])


def reload_replaceme1(ctxt):
    d = Div(class_='row', id='replaceme1')
    with d.divc('col'):
        d.h1(f'Head1 {now_ymdhms()}')
        with d.tablec():
            with d.thead():
                d.tr(Q.th(['Col2', 'Col2']))
            with d.tbody():
                for row in range(random.randrange(1,6)):
                    d.tr(Q.td([f'Row{row}', f'Row{row}']))
    print(d.render())


def reload_replaceme2(ctxt):
    d = Div(class_='row', id='replaceme2')
    with d.divc('col'):
        d.h1(f'Head2 {now_ymdhms()}')
        with d.tablec():
            with d.thead():
                d.tr(Q.th(['Col2', 'Col2']))
            with d.tbody():
                for row in range(random.randrange(1,10)):
                    d.tr(Q.td([f'Row{row}', f'Row{row}']))
    print(d.render())


def view(p, ctxt):
    with p.div(id='replaceme1'):
        p.h3('Waiting1...')

    with p.div(id='replaceme2'):
        p.h3('Waiting2...')

    p.reload_div('replaceme1', 'action/reload_div.py', 20)
    p.reload_div('replaceme2', 'action/reload_div.py', 10)
    for row in range(3):
        with p.divc('row'):
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}01', refresh=7, title=f'Chart {row} 1')
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}02', refresh=11, title=f'Chart {row} 2')
            with p.divc('col-4'):
                p.hc_live('reload_chart', 'live001', f'{row}03', refresh=13, title=f'Chart {row} 3')

default = view