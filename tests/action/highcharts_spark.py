from minimarkup.highcharts import hc_onoffdata
from .testdata import color, onoff_data

def default(p, ctxt):
    p.h1(ctxt.YMD)
    for nn in range(6):
        with p.div(class_='row'):
            with p.div(class_='col-md-1'):
                p.p(f'Title {nn}', class_='text-warning text-xs-center')
            with p.div(class_='col-md-10'):
                p.hc_spark(
                    series=dict(name=f'title{nn}',
                                color=color(),
                                data=hc_onoffdata(onoff_data(f'{ctxt.YMD}-000000', 10, mingap=60))),
                    height=48, max_=1) 
