"""
Simple table with filers to show/hide based on Type.
"""
from random import choices, sample, randint

from pybs4markup import toggle_buttons


def random_row(name, cols):
    """
    """
    return [name] + sample(range(12), cols)


def get_data(ctxt):
    """
    """
    data = []
    types = set()
    for idx in range(randint(10, 50)):
        for name, cols in choices([('short', 3), ('medium', 6), ('long', 9), ('slim', 1), ('empty', 0)]):
            row = random_row(name, cols)
            data.append([idx] + row)
            types.add(row[0])
    return sorted(types), data


def table_row_callback(row, last_row):
    """
    """
    return dict(class_=row[1])  # class name for hide/show


def table_col_callback(row, idx, last_row):
    """
    """
    if idx == 0:  # hide the index column
        return False


def select_col_callback(row, idx, last_row):
    """
    """
    return toggle_buttons(row[idx])


def default(p, ctxt):
    """
    Display a table with rows that can be filtered by Type.
    """
    types, data = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h1('Select + or - to Hide/Show different Types')
            p.simple_table([types], header=['Select'], col_callback=select_col_callback)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.simple_table(data, header=['Type', 'Data...'], row_callback=table_row_callback, col_callback=table_col_callback)
