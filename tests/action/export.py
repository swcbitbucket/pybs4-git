import glob

def files(ctxt):
    return dict(result=glob.glob(f'{ctxt.FOLDER}/*'))


def file(ctxt):
    return open(ctxt.PATH).read()
