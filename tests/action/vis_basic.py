import datetime


def default(p, ctxt):
    ymd = ctxt.YMD
    p.h1(ymd)

    t = datetime.datetime.now()
    dts = [t + datetime.timedelta(minutes=n) for n in range(0, 30, 3)]

    p.vis_timeline({t: v for t, v in zip(dts, ['Robert']*10)})
