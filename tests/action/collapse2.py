"""
"""
from random import choices, sample, randint

from pybs4.util.logging import error

from pybs4markup import Q, HIDE, toggle_buttons


def random_row(name, cols):
    """
    """
    return [name] + sample(range(12), cols)


def get_data(ctxt):
    """
    """
    data = []
    for idx in range(randint(10, 50)):
        for name, cols in choices([('short', 3), ('medium', 6), ('long', 9), ('slim', 1), ('empty', 0)]):
            data.append([idx] + random_row(name, cols))
            data.append([idx] + random_row('hidden', cols))  # return an extra hidden row after every row 
    return data


def row_callback(row, last_row):
    """
    """
    row_type = row[1]
    result = dict(class_=row_type)
    if row_type == 'hidden':
        result['style'] = HIDE
        result['class_'] += f' {row[0]}-hidden'
    return result


def col_callback(row, idx, last_row):
    """
    """
    if idx == 0:  # hide the index column
        return False
    row_type = row[1]
    if row_type == 'hidden' and idx == 1:
        return f'Index {row[0]} {last_row[1]}'
    col = row[idx]
    if row_type != 'hidden' and idx == 1:  # example of hover for info and hide/show of extra row
        name = f'{row[0]}-hidden'
        return toggle_buttons(label=row[1], name=name, btn='btn-warning btn-block', initial='+', title=f'Press to show hidden row Index {row[0]}')
    return col


def default(p, ctxt):
    """
    Display a table with hidden rows.
    """
    data = get_data(ctxt)

    with p.divc('row'):
        with p.divc('offset-2 col-8'):
            p.h1('Click Type to see the extra info on each row')
            p.simple_table(data, header=['Type', 'Data...'], row_callback=row_callback, col_callback=col_callback, table_classes='table-bordered table-sm')
