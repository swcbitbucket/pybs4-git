def default(p, ctxt):
    with p.divc('jumbotron'):
        p.h1(f'Hello {ctxt.USER} Options')
        p.h2(f'Options')
        p.p('Watch the selected options change here.', class_='lead')
        p.hr(class_='my-4')
        with p.ul():
            p.li(f'Env:{ctxt.ENV}')
            p.li(f'Ver:{ctxt.VER}')
            p.li(f'Tim:{ctxt.TIM}')
            p.li(f'Date:{ctxt.DMY}')
        p.hr(class_='my-4')
        p.h4(f'Today? {ctxt.TODAY}')
        p.h4(f'From {ctxt.YMD_MIN} to {ctxt.YMD}')
        p.h4(f'From {ctxt.DMY_MIN} to {ctxt.DMY}')
        p.h4(f'{ctxt.YMD_DAYS} day(s)')
