def default(p, ctxt):
    p.h3('It is easy to cut and paste highcharts examples!')
    p.hc_jinja('''
    chart: {
        type: 'boxplot'
    },
    title: {
        text: '{{ title|default('Chart Title') }}'
    },
    legend: {
        enabled: false
    },
    xAxis: {
        categories: {{ xcategories }},
        title: {
            text: '{{ xtitle|default('X Axis Title') }}'
        }
    },
    yAxis: {
        title: {
            text: '{{ ytitle|default('Y Axis Title') }}'
        },
        plotLines: [{
            value: {{ mean }},
            color: 'red',
            width: 2,
            label: {
                text: 'Mean: {{ mean }}',
                align: 'center',
                style: {
                    color: 'gray'
                }
            }
        }]
    },
    series: [{
        name: 'Box data',
        data: {{ boxdata }},
        tooltip: {
            headerFormat: 'Cat: {point.key}<br/>'
        }
    }, {
        name: 'Outlier',
        color: Highcharts.getOptions().colors[0],
        type: 'scatter',
        data: {{ outlierdata }},
        marker: {
            fillColor: 'white',
            lineWidth: 1,
            lineColor: Highcharts.getOptions().colors[0]
        },
        tooltip: {
            pointFormat: '({point.x}, {point.y})'
        }
    }]
    ''',
    mean = 900,
    xcategories = ['1', '2', '3', '4', '5'], #  X Axis labels
    boxdata = [ # min, lower quartile, median, upper quartile, max
        [760, 801, 848, 895, 965],
        [733, 853, 939, 980, 1080],
        [714, 762, 817, 870, 918],
        [724, 802, 806, 871, 950],
        [834, 836, 864, 882, 910],
        ],
    outlierdata = [ # x, y positions where 0 is the first category
        [0, 644],
        [4, 718],
        [3, 1100],
        [4, 951],
        [4, 969],
        ],
        )