# Extend menu1 with 2 more submenus
from pybs4.menu import Menu, Submenu, Action, Gosub
from pybs4.web import start

MENU = Menu(
    
    Submenu('Example-2',
        Action('Internal', 'internal.py'),
        Action('External', 'external.py'),
        Gosub('System'),
        Gosub('Test'),
    ),

    Submenu('System',
        Action('Performance', 'performance.py'),
        Action('Errors', 'errors.py'),
    ),

    Submenu('Test',
        Action('Status', 'test_status.py'),
        Action('Results', 'test_results.py'),
    ),

)

if __name__ == '__main__':
    start(MENU)
