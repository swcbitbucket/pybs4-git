# Extend menu2.py with dropdown + external url
from pybs4.menu import Menu, Submenu, Action, Gosub, Dropdown, Header, Divider
from pybs4.web import start

MENU = Menu(
    
    Submenu('Example-3',
        Action('Internal', 'internal.py'),
        Action('External', 'external.py'),
        Gosub('System'),
        Gosub('Test'),
        Dropdown('Help',
            Header('Info'),
            Action('About', 'about.py'),
            Divider(),
            Gosub('Documentation')),
    ),

    Submenu('System',
        Action('Performance', 'performance.py'),
        Action('Errors', 'errors.py'),
    ),

    Submenu('Test',
        Action('Status', 'test_status.py'),
        Action('Results', 'test_results.py'),
    ),

    Submenu('Documentation',
        Action('Docs', 'docs.py'),
        Action('Credits', 'http://swevolution.co.uk/logo'), # external URL in iframe
    ),

)

if __name__ == '__main__':
    start(MENU)
