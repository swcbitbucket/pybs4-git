from pybs4.requests import Requests

URL = 'http://pbs4.swevolution.co.uk'
URL = 'http://localhost:8080'
AUTH = ('admin@admin', 'admin')

r = Requests(URL, AUTH)
print(r.json('test_json_url', 'default', FILE='.py'))

print(r.json('dataview', call='POST', NAME='fred'))
print(r.json('dataview', call='PUT', NAME='fred', VALUE='99'))
print(r.json('dataview', call='DELETE', NAME='fred'))
