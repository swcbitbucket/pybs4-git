"""
This site uses the test ldap provided by https://github.com/rroemhild/docker-test-openldap.
see site for downloading and running ldap server with docker prior to running this site
"""
import platform
import cherrypy
from pybs4.web import start

from test_site import MENU

cherrypy.config.update({
    'LDAP_SERVER': 'localhost',
    'LDAP_PORT': 10389,
    'LDAP_BASEDN': 'dc=planetexpress,dc=com',
    'LDAP_AUTH_BASEDN': 'ou=people,dc=planetexpress,dc=com',
    })

if __name__ == '__main__':
    start(MENU, localport=8081, LIVE='webfaction' in platform.node(), liveport=25088, ldap=True)
