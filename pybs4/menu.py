"""
"""
import os

from pybs4.util.files import read
from pybs4.util.logging import error, cplog
from pybs4.datetime import OPTIONS


class Dropdown:
    """
    """

    def __init__(self, name, *options):
        """
        """
        self.name = name
        self.options = options

    def __iter__(self):
        """
        """
        for _ in self.options:
            yield _

    def __repr__(self):
        """
        """
        return f"{self.__class__.__name__}('{self.name}', {self.options})"


class Option(Dropdown):
    """
    """
    pass


class Header:
    """
    """

    def __init__(self, header):
        """
        """
        self.header = header

    def __repr__(self):
        """
        """
        return f"Header('{self.header}')"


class Divider:
    """
    """

    def __repr__(self):
        """
        """
        return "Divider()"


class Gosub:
    """
    """

    def __init__(self, name):
        """
        """
        self.name = name

    def __repr__(self):
        """
        """
        return f"Gosub('{self.name}')"


class Action:
    """
    """
    args = dict()
    roles = dict()
    trust = list()

    def __init__(self, label, target, role=None, trusted=False, default=False, **args):
        """
        """
        self.label = label
        self.target = target
        self.role = role
        self.trusted = trusted
        self.default = default
        if role:
            self.roles.setdefault(target, set()).add(role)
            Menu.roles.add(role)
        key = f'{label}:{target}'
        if key not in self.args:
            self.args[key] = args
        else:
            assert self.args[key] == args
        if trusted:
            self.trust.append(target)

    def __str__(self):
        """
        """
        return self.target

    def __repr__(self):
        """
        """
        return f"Action('{self.label}', {self.target})"

    @property
    def own_args(self):
        """
        """
        return self.get_args(str(self)) or ''

    @classmethod
    def get_type(cls, txt):
        """
        """
        if txt is None:
            txt = ''
        # Return type and target of the Action
        if isinstance(txt, list):  # during test this can be seen as a list
            error(f'unexpected action {txt}')
            txt = txt[-1]
        if txt.startswith('Json('):
            return 'Json', txt[5:-1]
        if txt.startswith('Load('):
            return 'Load', txt[5:-1]
        target = txt
        if '(' in txt:
            args = txt[:-1].split('(')[1]
            label, target = args.split(', ')
        atype = 'Url'
        if target.endswith('.py'):  # or Python...
            atype = 'Inline'
            if 'frame' in target:
                atype = 'Frame'
        return atype, target

    @classmethod
    def get_args(cls, txt):
        """
        # Return any arguments specified by the Action
        """
        if txt is None or '.py' not in txt or '(' not in txt:
            return None
        # Expect a repr string
        # debug(txt)
        args = txt[:-1].split('(')[1]
        label, target = args.split(', ')
        return cls.args[f'{label[1:-1]}:{target}']

    @classmethod
    def get_roles(cls, txt):
        """
        # Find any roles asociated with this target (Maybe none)
        """
        # debug(f'get_roles {txt}')
        atype, target = cls.get_type(txt)
        return str(cls.roles.get(target, None))

    @classmethod
    def istrusted(cls, txt):
        """
        # See if the target is trusted
        """
        atype, target = cls.get_type(txt)
        return target in cls.trust


class Submenu:
    """
    """
    roles = dict()

    def __init__(self, name, *fields, role):
        """
        # name is unuque in the site
        # fields can be GoSub, Action, Dropdown
        # role can be None for no checks or a named role (string)
        """
        self.sub = list()
        self.name = name
        self.default = None
        self.role = role
        if role:
            self.roles.setdefault(name, set()).add(role)
            Menu.roles.add(role)
        for f in fields:
            self.sub.append(f)

    def __iter__(self):
        """
        """
        for _ in self.sub:
            yield _


class Menu:
    """
    """
    roles = set()  # all known roles

    def __init__(self, *fields, noopt=False, sitemap=True):
        """
        # set noopt True to avoid any options (including calendar)
        # set sitemap False if you don't want one
        """
        self.name = None
        self.sub = dict()
        self.opt = list()
        self.noopt = noopt
        self.sitemap = sitemap
        for f in fields:
            if isinstance(f, Option):
                self.opt.append(f)
            else:
                assert isinstance(f, Submenu), f'Menu only expects Submenu or Option found {type(f)}'
                if self.name is None:
                    self.name = f.name
                assert f.name not in self.sub, f'Submenu names must be unique {f.name} already used'
                self.sub[f.name] = f
        self.opt.append(Option('Tim', *OPTIONS))
        self.validate()

    def validate(self):
        """
        """
        assert self.sub, 'Menu has no submenus'
        for sub in self.sub.values():
            cplog(f'{sub.name} Role:{sub.role}-->')
            for m in sub:
                if isinstance(m, Gosub):
                    cplog(f'\t{m}')
                    assert m.name in self.sub, f'Gosub Submenu {m.name} not found'
                elif isinstance(m, Dropdown):
                    cplog(f'\t{m.name}-->')
                    for d in m:
                        cplog(f'\t\t{repr(d)}')
                        if isinstance(d, Gosub):
                            assert d.name in self.sub, f'Gosub Submenu {d.name} not found'
                        elif isinstance(d, (Header, Divider)):
                            pass
                        else:
                            if sub.role and hasattr(m, 'target'):  # Flow the roles down to the actions
                                Action.roles.setdefault(m.target, set()).add(sub.role)
                            assert isinstance(d, Action), 'SubMenu Dropdown only expects Action or Gosub'
                            if d.default and not sub.default:
                                sub.default = d
                else:
                    if sub.role and hasattr(m, 'target'):  # Flow the roles down to the actions
                        Action.roles.setdefault(m.target, set()).add(sub.role)
                    cplog(f'\t{repr(m)} {m.own_args}')
                    assert isinstance(m, Action), f'SubMenu only expects Action, Gosub or Dropdown found {type(m)}'
                    if m.default and not sub.default:
                        sub.default = m

        if not self.noopt:
            for opt in self.opt:
                cplog(f'{opt.name}:')
                for val in opt:
                    cplog(f'\t{val}')

        cplog('Action args:')
        for a in Action.args:
            if Action.args[a]:
                cplog(f'\t{a}, {Action.args[a]}')

        cplog('Trusted Actions:')
        for a in Action.trust:
            cplog(f'\t{a}')

        cplog('Default Actions:')
        for sub in self.sub.values():
            if sub.default:
                cplog(f'\t{sub.name} {repr(sub.default)}')

        cplog('Submenu roles:')
        for submenu in sorted(Submenu.roles):
            cplog(f'\t{submenu} {",".join(sorted(Submenu.roles[submenu]))}')
        cplog('Action roles:')
        for target in sorted(Action.roles):
            cplog(f'\t{target} {",".join(sorted(Action.roles[target]))}')

        if os.path.exists('roles.tsv'):
            fileroles = [row.Role for row in read('roles.tsv')]
            for role in sorted(Menu.roles):
                if role not in fileroles:
                    error(f'{role} not defined in roles.tsv')
