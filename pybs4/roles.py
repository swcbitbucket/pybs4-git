"""
"""

import os

from pybs4.util.files import read, write
from pybs4.util.logging import debug

if not os.path.exists('user_roles.tsv'):
    write('user_roles.tsv', cols='User\tRole')
if not os.path.exists('roles.tsv'):
    write('roles.tsv', cols='Role')


def user_roles(user):
    result = set()
    for row in read('user_roles.tsv'):
        if user == row.User:
            result.add(row.Role)
    debug(f'User check {user} has {result}')
    return result


def has_role(user, roles):
    # Can be called with strings (from action)
    if isinstance(roles, str):
        if roles == 'None':
            roles = None
        else:  # {'SPECIAL', 'ADMIN'}
            roles = set([_ for _ in roles[1:-1].replace("'", '').replace(' ', '').split(',')])
    if user is None:
        # debug(f'Role check {user}+{roles} main menu')
        return True
    if roles is None:
        # debug(f'Role check {user} none to check')
        return True
    if roles.issubset(user_roles(user)):
        debug(f'Role check {user}+{roles} PASS')
        return True
    debug(f'Role check {user}+{roles} FAIL')
    return False
