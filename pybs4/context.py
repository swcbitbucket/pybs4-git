"""
"""
from pybs4.util.markup import Page as MPage
from pybs4.util.markup import Div as MDiv
from pybs4.util.datetime import today_ymd, diff_ymd, ymd2dmy, inputdate2ymd
from pybs4.util.logging import debug

from pybs4.datetime import adjust_ymd, min_ymd
from pybs4.menu import Action


def safeurl(url):
    """
    """
    result = []
    for c in str(url):
        if ord(c) <= 32:
            result.append(f'%{format(ord(c), "02X")}')
        else:
            result.append(c)
    return ''.join(result)


class _Context(dict):
    """
    # The purpose of this class is present a nice syntax to access the HTML shared variables
    # A helper class for local and remote versions
    """

    def __init__(self, **k):
        """
        """
        super().__init__(k)

    def consume(self, tag):
        """
        # Use this to make sure the variable does not get used again by mistake
        """
        return self.pop(tag, None)

    def consume_all(self, prefix):
        """
        # Use this to remove certain values eg NEW_ etc
        """
        for key in list(self):
            if key.startswith(prefix):
                self.pop(key)

    def consume_flag(self, tag):
        """
        # Use this for named inputs with no value (eg submit)
        """
        if tag in self and self[tag] == '':
            self.pop(tag)
            return True
    buttonclicked = consume_flag  # better name?
    isflag = consume_flag  # better name?

    def __getattr__(self, tag):
        """
        # allow access by attribute return None if missing
        # Convert to bools and None
        """
        result = self.get(tag, None)
        if result == 'True':
            return True
        if result == 'False':
            return False
        if result == 'None':
            return None
        return result

    def __repr__(self):
        """
        """
        result = []
        for k, v in sorted(self.items()):
            result.append(f'{k}={v}')
        return f'{self.__class__.__name__} {" ".join(result)}'


class WebContext(_Context):
    """
    # Used by web.py
    """
    SYSTEM = ['USER', 'YMD', 'MENU', 'ACTION', 'DMY', 'TODAY', 'YMD_MIN', 'DMY_MIN', 'YMD_DAYS', 'ROLES']

    def __init__(self, menu, **k):
        """
        """
        super().__init__(**k)
        self.menu = menu
        navigating = False
        debug('=' * 80)
        debug(f'Before {self}')

        # Navigation
        if 'CLEAR' in self:
            self.clear()
        elif 'HOME' in self:
            self.pop('HOME')
            self.pop('MENU', None)
            self.pop('ACTION', None)
            navigating = True
        elif 'GOSUB' in self:
            self['MENU'] = self.pop('GOSUB')
            self.pop('ACTION', None)
            self.pop('NEW_ACTION', None)
            navigating = True
        elif 'NEW_ACTION' in self:
            self['ACTION'] = self.pop('NEW_ACTION')
            navigating = True

        if self.get('ACTION', None) == 'None':
            self.pop('ACTION')

        # set/adjust YMD - its easier to work with then DMY
        TODAYYMD = today_ymd()
        if 'YMD' not in self:
            self['YMD'] = TODAYYMD

        if 'NEW_DMY' in self:
            self['YMD'] = inputdate2ymd(self.pop('NEW_DMY'))
            navigating = True

        if 'DMYLAST' in self:
            self['YMD'] = adjust_ymd(self['YMD'], self['TIM'], -1)
            self.pop('DMYLAST')
        elif 'DMYNEXT' in self:
            self['YMD'] = adjust_ymd(self['YMD'], self['TIM'], +1)
            self.pop('DMYNEXT')

        # Never in the future
        if self['YMD'] > TODAYYMD:
            self['YMD'] = TODAYYMD

        # Add in default values for any missing menu items and update any values with NEW_
        if 'MENU' not in self:
            self['MENU'] = menu.name
        for opt in menu.opt:
            name = opt.name.upper()
            if name not in self:
                self[name] = opt.options[0]
            newname = f'NEW_{name}'
            if newname in self:
                navigating = True  # clear user variables
                self[name] = self.pop(newname)
            assert self[name] in opt.options, self[name]

        if navigating:
            # Only remember context
            for key in list(self.keys()):
                # These are all set above (preserve)
                if key in ['USER', 'YMD', 'MENU', 'ACTION']:
                    continue
                # These are the menu options (preserve)
                if key in [opt.name.upper() for opt in menu.opt]:
                    continue
                # forget any others
                self.pop(key)

        # These are the system constants (read only)
        self['DMY'] = ymd2dmy(self['YMD'])
        self['TODAY'] = self['YMD'] == TODAYYMD

        # Fix start date and other useful bits
        self['YMD_MIN'] = min_ymd(self['YMD'], self['TIM'])
        self['DMY_MIN'] = ymd2dmy(self['YMD_MIN'])
        self['YMD_DAYS'] = diff_ymd(self['YMD'], self['YMD_MIN']).days + 1

        debug(f'After  {self}')

    def add_args(self, args):
        """
        # Add arguments specified in the menu
        """
        if args is not None:
            for k, v in args.items():
                k = k.upper()
                # do not overwrite system
                if k in self.SYSTEM:
                    continue
                self[k] = v

    @property
    def export(self):
        """
        # return a dictionary suitable for import by ActionContext
        # Its useful to clearly separate the different types
        """
        result = dict(SYSTEM_={}, OPT_={}, USER_={})
        opt_keys = [opt.name.upper() for opt in self.menu.opt]
        for key, val in self.items():
            assert key[-1] != '_', key
            if key in self.SYSTEM:
                result['SYSTEM_'][key] = val
                result[key] = val
            elif key in opt_keys:
                result['OPT_'][key] = val
                result[key] = val
            else:
                result['USER_'][key] = val
                result[key] = val
        return result

    @property
    def hidden_items(self):
        """
        # For use in web.py form
        """
        yield 'MENU', self.MENU
        if self.ACTION:
            yield 'ACTION', self.ACTION
        yield 'YMD', self.YMD
        ctxt = self.export
        for k, v in ctxt['OPT_'].items():
            yield k, v
        for k, v in ctxt['USER_'].items():
            yield k, v

    @property
    def minimal_url(self):
        """
        # For use in web.py shared links
        """
        result = list()
        result.append(f'MENU={self.MENU}')
        if self.ACTION:
            result.append(f'ACTION={self.ACTION}')
        result.append(f'YMD={self.YMD}')
        ctxt = self.export
        for k, v in ctxt['OPT_'].items():
            result.append(f'{k}={v}')
        for k, v in ctxt['USER_'].items():
            result.append(f'{k}={v}')
        return safeurl('&'.join(result))

    @property
    def url(self):
        """
        """
        minimal_url = self.minimal_url
        if 'ACTION' not in self:
            return f'menu?{minimal_url}'
        atype, atarget = Action.get_type(self.ACTION)
        if atype == 'Url':
            return f'{atarget}'
        assert atype in ('Frame', 'Inline'), f'unexpected type {atype}'
        return f'menu?{minimal_url}'


class JsonContext(_Context):
    """
    """

    def __init__(self, **k):
        """
        """
        super().__init__(**k)
        debug('+' * 80)
        debug(f'Init {self}')

    @property
    def export(self):
        """
        # return a dictionary suitable for import by ActionContext
        # NOTE: json must not get passed the standard stuff - just ACTION
        """
        result = dict(SYSTEM_={}, OPT_={}, USER_={})
        for key, val in self.items():
            assert key[-1] != '_', key
            result['USER_'][key] = val
            result[key] = val
        # debug(f'Export {self}')
        return result


class LoadContext(JsonContext):
    """
    """
    pass


class ActionContext(_Context):
    """
    # The context used by action.py
    """

    def __init__(self, **k):
        """
        # unpack the exported data saving the meta info
        """
        super().__init__(**k)
        self._system = self.pop('SYSTEM_')
        self._opt = self.pop('OPT_')
        self._user = self.pop('USER_')

    def _modified(self, **kwd):
        """
        # return a modified context with sutable safe guards
        """
        result = {}
        for key, val in self.items():
            if key in kwd and (key == 'ACTION' or key not in self._system):  # overwrite except r/o
                val = kwd[key]
            result[key] = val
        # add in any new values
        for key, val in kwd.items():
            assert key == key.upper(), f'expected uppercase {key}'
            if key not in self:
                result[key] = val
        # Remove anything that can get re-calculated on call back
        result.pop('DMY', None)
        result.pop('DMY_MIN', None)
        result.pop('TODAY', None)
        result.pop('YMD_DAYS', None)
        result.pop('YMD_MIN', None)
        return result.items()

    def _hidden(self, p, **kwd):
        """
        # add in hidden inputs for use in a form
        # p is a page
        # kwds can be used to modify the context
        """
        for key, val in self._modified(**kwd):
            p.input(type_='hidden', name=key, value=val)

    def _submit(self, p, label, **kwd):
        """
        # make a form and a submit button
        # p is a page
        # label is the button label
        # kwds can be used to modify the context
        """
        method = 'iframe' if 'frame' in self.get('ACTION', 'action') else 'action'
        with p.form(action=f'/{method}', method='post'):
            self._hidden(p, **kwd)
            p.button(label, class_='btn btn-primary btn-block', type_='submit')

    def href(self, **kwd):
        """
        # Return a nice href string for use in links and forms
        """
        method = 'iframe' if 'frame' in self.get('ACTION', 'action') else 'action'
        kv = []
        for key, val in self._modified(**kwd):
            kv.append(f'{key}={safeurl(val)}')
        return f'/{method}?{"&".join(kv)}'

    def refresh(self, secs, **kwd):
        """
        """
        return f'{secs} {self.href(**kwd)}'

    def _link(self, p, label, **kwd):
        """
        # Make an anchor + href
        # p is a page
        # label is the button label
        # kwds can be used to modify the context
        """
        p.a(label or None, href=self.href(**kwd), class_='hideonclick')

    def _visualise(self, p):
        """
        # For debug purposes
        """
        result = []
        for k, v in sorted(self.items()):
            key = []
            if k in self._system:
                k = f'SYSTEM {k}'
            elif k in self._opt:
                k = f'OPTION {k}'
            elif k in self._user:
                k = f'USER   {k}'
            else:
                k = f'?????? {k}'
            key.append(f'{k}={v or "True"}')
            result.append(''.join(key))
        p.pre('\n'.join(sorted(result)))


class Ctxtaddin:
    """
    # Helper class for writing scripts

    # Most methods have following syntax:
    # ctxt_command(CALL=, ACTION=, ARG1=...)
    # CALL = None calls 'default'
    # CALL specifies function to call
    # ACTION = None calls same sript
    # ACTION specifies remote script
    """

    def ctxt_visualise(self):
        """
        # Debug the context variables
        """
        self.ctxt._visualise(self)

    def ctxt_hidden(self, CALL=None, ACTION=None, **kwd):
        """
        # Lay down the set of context variables as hidden inputs in a form
        """
        if CALL is None:
            CALL = 'default'
        kwd['CALL'] = CALL
        if ACTION is not None:
            kwd['ACTION'] = ACTION
        self.ctxt._hidden(self, **kwd)

    def ctxt_link(self, label, CALL=None, ACTION=None, **kwd):
        """
        # Make a link with the current context variables
        """
        if CALL is None:
            CALL = 'default'
        kwd['CALL'] = CALL
        if ACTION is not None:
            kwd['ACTION'] = ACTION
        self.ctxt._link(self, label, **kwd)

    def ctxt_submit(self, label, CALL=None, ACTION=None, **kwd):
        """
        # Make a mini submit form a single button with the current context variables
        # NOTE: For styling put it in a div
        """
        if CALL is None:
            CALL = 'default'
        kwd['CALL'] = CALL
        if ACTION is not None:
            kwd['ACTION'] = ACTION
        self.ctxt._submit(self, label, **kwd)


class Page(MPage, Ctxtaddin):
    """
    """

    def __init__(self, ctxt, *a, **kwd):
        """
        """
        super().__init__(*a, **kwd)
        self.ctxt = ctxt


class Div(MDiv, Ctxtaddin):
    """
    """

    def __init__(self, ctxt, *a, **kwd):
        """
        """
        super().__init__(*a, **kwd)
        self.ctxt = ctxt
