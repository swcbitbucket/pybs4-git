"""
LDAP config is stored in cherrypy.config
Currently just a checkpassword operation and would use local role based permissions with the username.
"""
import cherrypy
from ldap3 import Server, Connection, ALL

cherrypy.config.setdefault('LDAP_SERVER', 'localhost')
cherrypy.config.setdefault('LDAP_PORT', 389)
cherrypy.config.setdefault('LDAP_CONNECT_TIMEOUT', 10)
cherrypy.config.setdefault('LDAP_USE_SSL', False)


class LDAPConn:
    server = Server(host=cherrypy.config['LDAP_SERVER'],
                    port=cherrypy.config['LDAP_PORT'],
                    use_ssl=cherrypy.config['LDAP_USE_SSL'],
                    connect_timeout=cherrypy.config['LDAP_CONNECT_TIMEOUT'],
                    get_info=ALL)


def checkpassword(realm, username, password):
    """
    attempt to bind to the ldap server using the provided credentials
    """
    user_string = f'cn={username},{cherrypy.config["LDAP_AUTH_BASEDN"]}'
    conn = Connection(LDAPConn.server, user_string, password)
    authenticated = conn.bind()
    conn.unbind()
    return authenticated
