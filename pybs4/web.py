"""
"""
import os
import tempfile
import json
import platform

from traceback import print_exc
from random import sample

import cherrypy

from pybs4.util.files import yield_namedtuple
from pybs4.util.logging import debug, error, log

from pybs4.menu import Action
from pybs4.context import WebContext, JsonContext, LoadContext
from pybs4.action import call as direct_call
from pybs4.web_view import View

major, minor, patch = platform.python_version_tuple()
PYTHON = f'python{major}.{minor}'


def spawn(ctxt):
    """
    """
    if Action.istrusted(ctxt.ACTION):
        # Make a direct call to the action function as part of the application
        # Used for non-interactive calls eg json services.
        return direct_call(ctxt.export)
    # call action.py passing the context via a temp file
    handle = tempfile.NamedTemporaryFile('w')
    try:
        export = ctxt.export
    except Exception as msg:
        print_exc()
        error(f'spawn export exception {msg}')
        return
    try:
        dumps = json.dumps(export)
    except Exception as msg:
        print_exc()
        error(f'spawn dumps exception {msg}')
        return
    print(dumps, file=handle)
    handle.flush()
    cmd = f'{PYTHON} -m pybs4.action {handle.name}'
    # debug(f'{cmd} # {ctxt.ACTION}')
    result = ''.join(os.popen(cmd).readlines())
    handle.close()
    return result


class Control:
    """
    """

    def __init__(self, menu):
        """
        """
        self._menu = menu

    @cherrypy.expose
    def default(self, *a, **k):
        """
        Hrefs to local html files come through here.
        """
        afile = '/'.join(a)
        if not k and afile and os.path.exists(afile) and '.htm' in afile:
            return open(afile).read()  # render the page directly no more processing needed
        error(f'default UNEXPECTED {a} {k}')
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def action(self, *action, **args):
        """
        """
        # debug(f'action {action} {args}')
        action = '/'.join(action)
        action = action.replace('.py', '')
        kwd = dict(ACTION=f'{action}.py')
        kwd.update(args)
        return self.menu(**kwd)

    @cherrypy.expose
    def submenu(self, menu):
        """
        """
        # debug(f'submenu {menu}')
        if menu not in self._menu.sub:
            error(f'submenu UNEXPECTED {menu}')
            raise cherrypy.HTTPRedirect('/')
        kwd = dict(GOSUB=menu)
        return self.menu(**kwd)

    def load_error(self, msg):
        """
        """
        msg = f'jquery load {msg}'
        error(msg)
        return ''

    @cherrypy.expose
    def load(self, *action, **args):
        """
        """
        # debug(f'load {action} {args}')
        cherrypy.response.status = 500  # Internal server errorr
        if cherrypy.request.method not in ('POST', 'GET'):
            return self.load_error(f'request {cherrypy.request.method} expected POST or GET')
        action = '/'.join(action)
        action = f"{action.replace('.py', '')}.py"
        if not (os.path.exists(action) or os.path.exists(f'action/{action}')):
            return self.load_error(f'{action} not found')
        args['ACTION'] = f'Load({action})'
        args['ROLES'] = Action.get_roles(args['ACTION'])
        args['USER'] = cherrypy.request.login
        ctxt = LoadContext(**args)
        try:
            result = spawn(ctxt)
        except Exception as msg:
            print_exc()
            return self.load_error(f'spawn fail {action} {ctxt} {msg}')
        cherrypy.response.status = 200  # OK
        return result
    text = load  # alternative better name?

    def json_error(self, msg):
        """
        """
        msg = f'json {msg}'
        error(msg)
        return dict(error=msg)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def json(self, *action, **args):
        """
        """
        # debug(f'json {action} {args}')
        cherrypy.response.status = 500  # Internal server errorr
        args.pop('_', None)  # cache veriable
        if cherrypy.request.method not in ('POST', 'GET'):
            return self.json_error(f'request {cherrypy.request.method} expected POST or GET')
        action = '/'.join(action)
        action = f"{action.replace('.py', '')}.py"
        if not (os.path.exists(action) or os.path.exists(f'action/{action}')):
            return self.json_error(f'{action} not found')
        args['ACTION'] = f'Json({action})'
        args['ROLES'] = Action.get_roles(args['ACTION'])
        args['USER'] = cherrypy.request.login
        ctxt = JsonContext(**args)
        try:
            result = spawn(ctxt)
        except Exception as msg:
            print_exc()
            return self.json_error(f'spawn fail {action} {ctxt} {msg}')
        try:
            result = json.loads(result)
        except Exception as msg:
            print_exc()
            return self.json_error(f'loads fail {action} {ctxt} {msg} {result}')
        cherrypy.response.status = result.pop('status', 200)
        return result

    @cherrypy.expose
    def menu(self, **kwd):
        """
        """
        # debug(f'menu {kwd}')
        if cherrypy.request.method not in ('POST', 'GET'):
            error(f'unexpected request method {cherrypy.request.method}')
            cherrypy.response.status = 405  # method not allowed
            return ''

        ctxt = WebContext(self._menu, **kwd)
        ctxt['USER'] = cherrypy.request.login
        if ctxt.ACTION:
            log(f'USER={ctxt.USER} URL={ctxt.minimal_url}')
            ctxt['ROLES'] = Action.get_roles(ctxt.ACTION)
            dtype, dtarget = Action.get_type(ctxt.ACTION)
            ctxt.add_args(Action.get_args(ctxt.ACTION))
            p = View(title=dtarget, hideonsubmit=dtype != 'Frame').menu(ctxt, self._menu, navigate=False)
            with p.main(role='main', class_='container-fluid hideonsubmit'):
                if dtype == 'Url':
                    # iframe given url
                    with p.divc('ratio ratio-1by1'):
                        p.iframe(None, src=dtarget)
                elif dtype == 'Frame':
                    # iframe called via hidden form and automated post to /iframe
                    with p.divc('ratio ratio-1x1'):
                        with p.form(id='directform', target='directiframe', method='post', action='/iframe'):
                            for k, v in ctxt.items():
                                p.hidden_input(k, v)
                        p.iframe(None, name='directiframe')
                        p.javascript_inline("$('#directform').submit()")
                elif dtype == 'Inline':
                    # Include page inline - maybe javascript
                    result = spawn(ctxt).strip()
                    if result.endswith('</script>'):
                        div, jscript = result[:-9].split('<script>', 1)
                        p += div
                        p.javascript_inline(jscript)  # Already stripped <script> and </script>
                    else:
                        p += result
                else:
                    p.p(f'{dtype} {dtarget} not found')
            return p.render()

        else:  # Submenu changes

            if self._menu.sub[ctxt.MENU].default:
                # Sneaky generate a dummy page and submit to get to the desired page
                p = View('redirect')
                with p.form(id='redirectform', method='post', action='/menu'):
                    for k, v in ctxt.hidden_items:
                        p.hidden_input(k, v)
                    p.hidden_input('NEW_ACTION', repr(self._menu.sub[ctxt.MENU].default))
                p.javascript_inline("$('#redirectform').submit()")
                return p.render()
            return View(title=ctxt.MENU).menu(ctxt, self._menu, navigate=True).render()

    @cherrypy.expose
    def index(self):
        """
        # Only come here from '/' anything else will be rejected
        """
        if cherrypy.request.method != 'GET':
            error(f'index UNEXPECTED {cherrypy.request.method}')
            cherrypy.response.status = 500
            return ''
        return View(title='Index').menu(WebContext(self._menu), self._menu, navigate=True).render()

    @cherrypy.expose
    def iframe(self, **kwd):
        """
        """
        assert cherrypy.request.method in ('GET', 'POST')
        ctxt = WebContext(self._menu, **kwd)
        ctxt['USER'] = cherrypy.request.login
        ctxt['ROLES'] = Action.get_roles(ctxt.ACTION)
        return spawn(ctxt)

    @cherrypy.expose
    def logout(self, **kwd):
        """
        """
        debug('logout handled')
        raise cherrypy.HTTPRedirect('/')


def checkpassword(realm, username):
    """
    """
    if 'logout' in cherrypy.url():
        debug('logout requested')
        return False
    realm = realm.lower()
    user = username.lower()
    for row in yield_namedtuple('users.htdigest'):
        if row.User == user and row.Realm == realm:
            return row.Password


def start(MENU, localport=8080, LIVE=False, liveport=None, ldap=False):
    """
    """
    if LIVE:
        assert liveport
        cherrypy.config.update({
            'environment': 'production',
            'log.screen': False,
            'server.socket_host': '127.0.0.1',
            'server.socket_port': liveport,
        })
    else:
        cherrypy.config.update({
            'server.socket_host': '0.0.0.0',
            'server.socket_port': localport,
            'log.screen': True,
        })

    if ldap:
        # import here to enable running without the ldap3 package unless running ldap auth
        from pybs4 import ldap_auth
        protect = {
            'tools.auth_basic.on': True,
            'tools.auth_basic.realm': 'live' if LIVE else 'local',
            'tools.auth_basic.checkpassword': ldap_auth.checkpassword,
            'tools.auth_basic.debug': False
        }
    else:
        protect = {
            'tools.auth_digest.on': os.path.exists('users.htdigest'),
            'tools.auth_digest.realm': 'live' if LIVE else 'local',
            'tools.auth_digest.get_ha1': checkpassword,
            'tools.auth_digest.key': ''.join(sample('1234567890abcdef', 16)) if LIVE else '1234567890abcdef',
            'tools.auth_digest.debug': False
        }

    STATIC_DIR = os.path.join(os.path.abspath('.'), 'static')
    if not os.path.isdir(STATIC_DIR):
        os.mkdir(STATIC_DIR)
    cherrypy.quickstart(Control(MENU), '/',
                        {
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': STATIC_DIR
        },
        # '/': unprotected,
        '/menu': protect,
        '/submenu': protect,
        '/action': protect,
        '/load': protect,
        '/text': protect,
        '/json': protect,
        '/iframe': protect,
        '/logout': protect,
    })
