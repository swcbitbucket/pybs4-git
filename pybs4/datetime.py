"""
"""

import datetime
from calendar import isleap, monthrange

from pybs4.util.datetime import date2ymd, ymd2date, add_days, today_ymd

OPTIONS = ('Day',
           'Week', 'This Week',
           'Month', 'This Month',
           'Quarter',  # 'This Quarter',
           'Year', 'This Year',
           'All')


def change_month(ymd, sign):
    """
    """
    assert abs(sign) == 1
    date = ymd2date(ymd)
    month = date.month
    while True:
        date += datetime.timedelta(days=sign)
        if date.month != month:
            day = monthrange(date.year, date.month)[1]
            return date2ymd(date.replace(day=day))


def add_one_month(ymd, sign):
    """
    # special treatment for 28/29 in Feb and 30/31 rest
    """
    assert abs(sign) == 1
    date = ymd2date(ymd)
    day = date.day
    month = date.month
    year = date.year

    # First get the month and year right
    if month == 1 and sign < 0:
        month = 12
        year -= 1
    elif month == 12 and sign > 0:
        month = 1
        year += 1
    elif sign > 0:
        month += 1
    else:
        month -= 1

    if day == 28:  # maybe last in Feb
        if (date.month == 2 or month == 2) and not isleap(year):
            day = monthrange(year, month)[1]

    elif day == 29:  # always the last in Feb
        if (date.month == 2 or month == 2):
            day = monthrange(year, month)[1]

    elif day == 30:  # 28/29/30 day months are last
        if date.month in (4, 6, 9, 11) or month in (2, 4, 6, 9, 11):
            day = monthrange(year, month)[1]

    elif day == 31:  # always last
        day = monthrange(year, month)[1]

    return date2ymd(datetime.date(year, month, day))


def add_one_quarter(ymd, sign):
    """
    """
    return add_one_month(add_one_month(add_one_month(ymd, sign), sign), sign)


def adjust_ymd(ymd, opt, sign):
    """
    # Adjust by +/-
    """
    if opt == 'Day':
        return date2ymd(ymd2date(ymd) + sign * datetime.timedelta(days=1))

    if opt == 'Week':
        return date2ymd(ymd2date(ymd) + sign * datetime.timedelta(days=7))

    if opt == 'Month':
        return add_one_month(ymd, sign)

    if opt == 'Quarter':
        return add_one_quarter(ymd, sign)

    if opt == 'Year':
        date = ymd2date(ymd)
        day = date.day
        if date.month == 2 and date.day == 29 and isleap(date.year):
            day = 28
        return date2ymd(date.replace(day=day, year=date.year + sign))

    if opt == 'All':
        return today_ymd() if sign > 0 else '20000101'

    if opt == 'This Week':
        start = ymd2date(ymd)
        while True:
            start = start + sign * datetime.timedelta(days=1)
            if start.weekday() == 6:  # Sunday
                return date2ymd(start)

    if opt == 'This Month':
        return change_month(ymd, sign)

    if opt == 'This Year':
        date = ymd2date(ymd)
        return date2ymd(date.replace(day=31, month=12, year=date.year + sign))

    raise Exception(opt)


def min_ymd(ymd, opt):
    """
    # Calculate the start date given the end and the opt
    """
    if opt == 'Day':
        return ymd
    if opt == 'Week':
        return date2ymd(ymd2date(ymd) - datetime.timedelta(days=6))
    if opt == 'Month':
        return add_days(add_one_month(ymd, -1), 1)
    if opt == 'Quarter':
        return add_days(add_one_quarter(ymd, -1), 1)
    if opt == 'Year':
        date = ymd2date(ymd)
        return add_days(date2ymd(date.replace(year=date.year - 1)), 1)

    if opt == 'This Week':
        start = ymd2date(ymd)
        while True:
            start = start - datetime.timedelta(days=1)
            if start.weekday() == 0:  # Monday
                return date2ymd(start)

    if opt == 'This Month':
        date = ymd2date(ymd)
        return date2ymd(date.replace(day=1))

    if opt == 'This Year':
        date = ymd2date(ymd)
        return date2ymd(date.replace(month=1, day=1))

    if opt == 'All':
        return '20000101'
