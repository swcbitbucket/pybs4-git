import datetime


def ymdhms2datetime(ymdhms):
    return datetime.datetime.strptime(ymdhms, '%Y%m%d-%H%M%S')


def datetime2ymdhms(datetime):
    return datetime.strftime('%Y%m%d-%H%M%S')


def now_ymdhms():
    return datetime2ymdhms(datetime.datetime.now())


def date2dmy(date):
    # NOTE:Works with date or datetime
    return date.strftime('%d/%m/%Y')


def date2ymd(date):
    # NOTE:Works with date or datetime
    return date.strftime('%Y%m%d')


def dmy2date(dmy):
    return datetime.datetime.strptime(dmy, '%d/%m/%Y').date()


def ymd2date(ymd):
    return datetime.datetime.strptime(ymd, '%Y%m%d').date()


def ymd2dmy(ymd):
    return date2dmy(ymd2date(ymd))


def dmy2ymd(dmy):
    return date2ymd(dmy2date(dmy))


def inputdate2ymd(ymd):
    return date2ymd(datetime.datetime.strptime(ymd, '%Y-%m-%d').date())


def dmy2inputdate(dmy):
    dd, mm, yyyy = dmy.split('/')
    return f'{yyyy}-{mm}-{dd}'


def diff_ymd(ymd1, ymd2):
    return ymd2date(ymd1) - ymd2date(ymd2)


def today_ymd():
    return date2ymd(datetime.date.today())


def add_days(ymd, days):
    return date2ymd(ymd2date(ymd) + datetime.timedelta(days=days))
