from jinja2 import Template, Environment, FileSystemLoader
ENV = Environment(loader=FileSystemLoader('jinja'))


def J(fname):
    return ENV.get_template(fname)