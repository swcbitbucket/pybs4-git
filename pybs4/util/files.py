import os
from glob import glob
from collections import namedtuple


def sglob(path):
    return [_ for _ in sorted(glob(path))]


def _delim(file):
    # return delimiter expected in a file
    delim = None
    if file.endswith('.csv'):
        delim = ','
    elif file.endswith('.tsv'):
        delim = '\t'
    elif file.endswith('.htdigest'):
        delim = ':'
    return delim


def split(file, cols):
    # given a file name and the cols return the cols list
    delim = _delim(file)
    return cols.split(delim)


def read_cols(file):
    # Line 1 contains the header
    for line in open(file):
        line = line[:-1] # skip \n
        return line.split(_delim(file))


def yield_namedtuple(file):
    # Read file and yield named tuples
    delim = _delim(file)
    ttype = None
    for line in open(file):
        line = line[:-1] # skip \n
        if ttype is None:
            ttype = namedtuple('namedtuple', line.split(delim))
        else:
            yield ttype._make(line.split(delim))


def read(file):
    # sorted no dupes
    return sorted(set([_ for _ in yield_namedtuple(file)]))


def write(file, data=None, cols=None):
    # sorted no dupes via tmp
    # data is None to init file
    # cols can be None if file exists
    delim = _delim(file)
    if cols is None:
        cols = read_cols(file)
    if isinstance(cols, str):
        cols= cols.split(delim)
    if data is None:
        data = []
    tmp = f'{file}_tmp'
    with open(tmp, 'w') as handle:
        print(delim.join(cols), file=handle)
        for line in sorted(set(data)):
            print(delim.join(line), file=handle)
    os.rename(tmp, file)
