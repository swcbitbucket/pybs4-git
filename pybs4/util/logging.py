import sys
import cherrypy


def cplog(msg):
    cherrypy.log(msg)


def _cplog(severity, msg):
    cherrypy.log(f'{severity} {msg}')


def log(msg):
    _cplog('LOG', msg)


def debug(msg):
    _cplog('DEBUG', msg)


def error(msg):
    _cplog('ERROR',msg)


def warning(msg):
    _cplog('WARNING', msg)

