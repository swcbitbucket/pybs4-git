"""
"""
from pybs4.util.files import read


def default(p, ctxt):
    """
    # Check users and roles to ensure inputs make sence!
    """
    users = set()
    for row in read('users.htdigest'):
        user = row.User
        users.add(user)
        if not row.Password:
            p.alert(f'User {user} has a blank passsword')
    roles = set([_.Role for _ in read('roles.tsv')])
    combined = read('user_roles.tsv')

    usedusers = set()
    usedroles = set()
    for row in combined:
        usedusers.add(row.User)
        usedroles.add(row.Role)
        if row.User not in users:
            p.alert(f'User {row.User} not found', 'danger')
        if row.Role not in roles:
            p.alert(f'Role {row.Role} not used', 'danger')

    for user in sorted(users - usedusers):
        p.alert(f'User {user} has no roles', 'warning')

    for role in sorted(roles - usedroles):
        p.alert(f'Role {role} has no users', 'info')

    p.p('Done')
