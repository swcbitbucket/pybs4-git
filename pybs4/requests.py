"""
"""

import requests

from pybs4.util.logging import error, debug


class Requests:
    """
    """

    def __init__(self, url, auth=None):
        """
        """
        self.URL = url
        self.AUTH = auth

        self.url = None
        self.params = None
        self.request = None

        self.status_code = None
        self._text = None
        self._json = None

    def __repr__(self):
        """
        """
        return f'{self.url} {self.params} Status:{self.status_code} Auth:{bool(self.AUTH)}'

    def _post(self, method, script, call, **params):
        """
        """
        self.url = f'{self.URL}/{method}/{script}'
        params['CALL'] = call
        self.params = params
        self.request = requests.post(self.url, params=self.params, auth=requests.auth.HTTPDigestAuth(*self.AUTH))
        self.status_code = self.request.status_code
        self._text = self.request.text
        if self.status_code != 200:
            error(self)
        elif 'DEBUG' in params:
            debug(self)

    def text(self, script, call, **params):
        """
        """
        self._post('text', script, call, **params)
        return self._text
    load = text  # Alternative better name?

    def json(self, script, call, **params):
        """
        """
        self._post('json', script, call, **params)
        self._json = {}
        try:
            self._json = self.request.json()
        except Exception as msg:
            error(f'{self} {msg}')
        return self._json
