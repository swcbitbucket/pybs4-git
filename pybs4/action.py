"""
The idea here is to import and call query/name.py with a context.
Designed to be called from pybs4.web.spawn (os.popen) for interactive scripts.
Or directly from pybs4.web for trusted scripts.
"""
import sys
import os
import json
from importlib import import_module
from inspect import signature
from traceback import print_exc

from pybs4.util.markup import Q
from pybs4.util.logging import error, debug

from .context import ActionContext, Page, Div
from .menu import Action
from .roles import has_role


def log_error(onerror):
    """
    # Log the error to the console and return for the real destination
    """
    error(onerror)
    return f'ERROR\n{onerror}'


def callload(callfunction, context, onerror):
    """
    """
    # No need to remember how we got here
    context.pop('ACTION')
    if str(signature(callfunction)) != '(ctxt)':
        return log_error(f'{onerror}Load bad signature')
    try:
        return callfunction(ctxt=context)
    except Exception as msg:
        print_exc()
        return log_error(f'{onerror}Load call(ctxt) Exception {msg}')


def calljson(callfunction, context, onerror):
    """
    """
    # No need to remember how we got here
    context.pop('ACTION')
    if str(signature(callfunction)) != '(ctxt)':
        return json.dumps(dict(error=log_error(f'{onerror}Json bad signature'), status=500))
    try:
        result = callfunction(ctxt=context)
    except Exception as msg:
        print_exc()
        return json.dumps(dict(error=log_error(f'{onerror}Json call(ctxt) Exception {msg}'), status=500))
    if not isinstance(result, dict):
        return json.dumps(dict(error=log_error(f'{onerror}Json expected dict found: {type(result)}'), status=500))
    try:
        return json.dumps(result)
    except Exception as msg:
        print_exc()
        return json.dumps(dict(error=log_error(f'{onerror}Json dumps Exception {msg}'), result=str(result), status=500))


def callframe(callfunction, context, onerror):
    """
    """
    # Either let the frame create its own page
    if str(signature(callfunction)) == '(ctxt)':
        try:
            return callfunction(ctxt=context)
        except Exception as msg:
            print_exc()
            return Q.pre(log_error(f'{onerror}Frame call(ctxt) Exception {msg}'), style='background:black;color:white')
    # Or create a standard page and pass it in
    p = Page(context)
    with p.divc('container-fluid hideonsubmit'):
        try:
            callfunction(p=p, ctxt=context)
        except Exception as msg:
            print_exc()
            p.pre(log_error(f'{onerror}Frame call(p, ctxt) Exception {msg}'))
    return p.render()


def callinline(callfunction, context, onerror):
    """
    # Create the div and pass it in
    """
    p = Div(context, class_='hideonsubmit')
    try:
        callfunction(p=p, ctxt=context)
    except Exception as msg:
        print_exc()
        p.pre(log_error(f'{onerror}Inline call(p, ctxt) Exception {msg}'))
    return p.render()


def report_error(isjson, onerror):
    """
    """
    error(onerror)
    if isjson:
        return json.dumps(dict(error=onerror, status=500))
    return Q.pre(onerror, style='background:black;color:white')


def call(kwds):
    """
    """
    context = ActionContext(**kwds)
    debug(context)

    qtype, qtarget = Action.get_type(context['ACTION'])
    context['ACTION'] = qtarget  # forget fancy menu signature (otherwise the menu args always get added in)

    # Pick out html files and render them immediateley
    if '.htm' in qtarget and os.path.exists(qtarget):
        return open(qtarget).read()

    call = context.pop('CALL', 'default')
    onerror = f'Call {qtarget}: def {call}(...)\n{context}\n'

    if '/' not in qtarget:
        qtarget = f'action/{qtarget}'

    isjson = qtype == 'Json'

    if not has_role(context.USER, context.pop('ROLES')):
        return report_error(isjson, f'{onerror}Access denied')

    qtarget = qtarget.replace('/', '.').replace('.py', '')

    try:
        m = import_module(f'{qtarget}')
    except Exception as msg:
        print_exc()
        return report_error(isjson, f'{onerror}Import fail {msg}')

    try:
        callfunction = getattr(m, call)
    except Exception as msg:
        print_exc()
        return report_error(isjson, f'{onerror}Call not found {msg}')

    if qtype == 'Load':
        return callload(callfunction, context, onerror)
    if isjson:
        return calljson(callfunction, context, onerror)
    if qtype == 'Frame':
        return callframe(callfunction, context, onerror)
    # Inline
    return callinline(callfunction, context, onerror)


if __name__ == '__main__':
    # When called from pybs4.web.spawn
    kwds = json.loads(open(sys.argv[1]).read())
    print(call(kwds))
