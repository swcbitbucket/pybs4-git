"""
This is the supporting view for web.py
"""

import os
import cherrypy

import markdown

from pybs4.util.markup import Page
from pybs4.util.logging import debug
from pybs4.util.datetime import dmy2inputdate

from pybs4.menu import Dropdown, Gosub, Action, Header, Divider, Submenu
from pybs4.roles import has_role


class View(Page):
    """
    """

    def __init__(self, title, hideonsubmit=True):
        """
        """
        # Add Top padding to start body after menu.
        super().__init__(title=title, hideonsubmit=hideonsubmit)
        self.javascript_inline('''
$(window).on('load', function () {$('body').css('padding-top', $('.navbar').height()+20);});
$(window).on('resize', function () {$('body').css('padding-top', $('.navbar').height()+20);});
''')

    def menu_link_button(self, name, key, title, value=None):
        """
        """
        self.button(name, class_='btn p-2 nav-link menu_link_button',
                    type_='submit', name=key.upper(), value=value,
                    date_toggle='tooltip', title=title)

    def menu_link_gosub(self, name, title):
        """
        """
        self.button(name, class_='btn p-2 nav-link menu_link_gosub',
                    type='submit', name='GOSUB', value=name,
                    date_toggle='tooltip', title=title)

    def option_link_button(self, name, key, title):
        """
        """
        self.button(name, class_='btn border border-secondary p-2 btn-opaque option_link_button',
                    type_='submit', name=key.upper(),
                    date_toggle='tooltip', title=title)

    def menu_dropdown_toggle(self, name):
        """
        """
        self.a(name, class_='nav-link dropdown-toggle p-2 menu_dropdown_toggle', role='button', data_bs_toggle='dropdown')

    def menu_dropdown_button(self, name, key, value, title):
        """
        """
        self.button(name, class_='dropdown-item menu_dropdown_button', type_='submit', name=key.upper(), value=value,
                    date_bs_toggle='tooltip', title=title)

    def menu_dropdown_gosub(self, name, title):
        """
        """
        self.button(name, class_='dropdown-item menu_dropdown_gosub',
                    type='submit', name='GOSUB', value=name,
                    date_bs_toggle='tooltip', title=title)

    def hidden_input(self, key, val):
        """
        """
        self.input(type_='hidden', name=key.upper(), value=val)

    def menu(self, ctxt, mainmenu, navigate):
        """
        # navigate = True (site navigation and markdown) or not
        """
        altmenu = ctxt.MENU != mainmenu.name
        usemenu = mainmenu.sub[ctxt.MENU]
        user = ctxt.USER

        with self.header():
            with self.form(action='/menu', method='post', class_='form-inline hideonsubmit'):
                for k, v in ctxt.hidden_items:
                    self.hidden_input(k, v)

                with self.nav(class_='navbar navbar-expand-md fixed-top bg-dark'):
                    with self.divc('container-fluid'):

                        if altmenu:
                            self.button(self.icon('bi-house-door-fill', color='white') + f' {usemenu.name}', class_='btn opacity-50 text-white navbar-brand', type_='submit', name='HOME')
                        else:
                            self.a(self.icon('bi-house-door-fill', color='white') + f' {usemenu.name}', role='button', class_='btn opacity-50 text-white navbar-brand hideonclick', href='/')

                        # dark + light to always display on inverse menus
                        self.button(self.icon('bi-list'), class_='navbar-toggler',
                                    type_='button', data_bs_toggle='collapse', data_bs_target='#mainnavbar')

                        with self.divc('navbar-collapse collapse', id='mainnavbar'):
                            with self.divc('me-auto'):
                                with self.ul(class_='navbar-nav me-auto mb-2 mb-md-0'):
                                    for menu in usemenu:
                                        if isinstance(menu, Gosub):
                                            if has_role(user, Submenu.roles.get(menu.name)):
                                                with self.li(class_='nav-item mr-1'):
                                                    self.menu_link_gosub(menu.name,
                                                                         title=f'goto submenu {menu.name}')
                                        elif isinstance(menu, Action):
                                            if has_role(user, Submenu.roles.get(menu.target)):
                                                with self.li(class_='nav-item mr-1'):
                                                    self.menu_link_button(menu.label, 'NEW_ACTION', value=repr(menu),
                                                                          title=f'do {menu.target}')
                                        elif isinstance(menu, Dropdown):
                                            with self.li(class_='nav-item dropdown mr-1'):
                                                self.menu_dropdown_toggle(menu.name)
                                                with self.divc('dropdown-menu'):
                                                    for option in menu:
                                                        if isinstance(option, Gosub):
                                                            if has_role(user, Submenu.roles.get(menu.name)):
                                                                self.menu_dropdown_gosub(option.name,
                                                                                         title=f'goto submenu {option.name}')
                                                        elif isinstance(option, Header):
                                                            self.h6(option.header, class_='dropdown-header')
                                                        elif isinstance(option, Divider):
                                                            self.hr(class_='dropdown-divider')
                                                        else:
                                                            if has_role(user, Submenu.roles.get(option.target)):
                                                                self.menu_dropdown_button(option.label, 'NEW_ACTION', repr(option),
                                                                                          f'do {option.target}')

                            if not mainmenu.noopt:
                                with self.divc('d-flex'):
                                    with self.ul(class_='navbar-nav'):
                                        for rhm in mainmenu.opt:
                                            self.span(f'{rhm.name}:', class_='navbar-text')
                                            with self.li(class_='nav-item dropdown mr-1'):
                                                chosen = ctxt.get(rhm.name.upper(), rhm.options[0])
                                                self.menu_dropdown_toggle(chosen)
                                                with self.divc('dropdown-menu'):
                                                    for option in rhm.options:
                                                        if option != chosen:
                                                            self.menu_dropdown_button(option, f'NEW_{rhm.name}', option,
                                                                                      f'set {rhm.name}')
                                        with self.li(class_='nav-item mr-1'):
                                            self.option_link_button(self.icon('bi-dash', color='white'), 'DMYLAST', title=f'back {ctxt.TIM}')
                                        with self.li(class_='nav-item mr-1'):
                                            self.input(onchange='submit()',
                                                       type_='date',
                                                       class_='form-control text-center p-2',
                                                       name='NEW_DMY',
                                                       value=dmy2inputdate(ctxt.get('DMY', 'TODAY')))
                                        with self.li(class_='nav-item mr-1'):
                                            self.option_link_button(self.icon('bi-plus', color='white'), 'DMYNEXT', title=f'forward {ctxt.TIM}')
                                            debug(f'cp.url {cherrypy.url()}')
                                            prefix = cherrypy.url()
                                            http, rest = prefix.split('://')
                                            prefix = f'{http}://{rest.split("/")[0]}'
                                            url = f'{prefix}/{ctxt.url}'
                                            debug(f'share {url}')
                                            self.button(self.icon('bi-share', color='white'), type_='button', class_='btn border border-secondary p-2 btn-opaque',
                                                        title='Share URL', data_bs_toggle='popover', data_content=url)

        if navigate:
            # Look for a markdown page
            mdfile = f'markdown/menu_{ctxt.MENU.lower().replace(" ","_")}.md'
            if os.path.exists(mdfile):
                with self.divc('jumbotron text-center hideonsubmit'):
                    self += markdown.markdown(open(mdfile).read())
            # Site navigation
            if not altmenu:
                self.sitemap(mainmenu)

        return self

    def _toggle_button(self, name, toggle):
        """
        """
        self.button(name, type='button', class_='btn btn-outline-primary btn-block', onclick=f"toggle_display('{toggle}')")

    def _submenu_link(self, name):
        """
        """
        with self.form(action='/menu', method='post'):
            self.button(name, class_='btn btn-outline-warning btn-block', name='GOSUB', value=name)

    def _action_link(self, submenu, action):
        """
        """
        with self.form(action='/menu', method='post'):
            self.hidden_input('MENU', submenu)
            self.button(action.label, class_='btn btn-outline-success btn-block', name='NEW_ACTION', value=repr(action))

    def sitemap(self, menu):
        """
        """
        if not menu.sitemap:
            return
        with self.divc('jumbotron offset-4 col-4 hideonsubmit'):
            self.h3('Site Map', class_='text-center')
            self.hr()
            for submenu in menu.sub:
                safemenu = submenu.replace(' ', '')
                with self.divc('row'):
                    with self.divc('col-12'):
                        self._toggle_button(submenu, safemenu)
                for content in menu.sub[submenu]:
                    if isinstance(content, Gosub):
                        with self.divc(f'row {safemenu}', style='display:none'):
                            with self.divc('offset-1 col-10'):
                                self._submenu_link(content.name)
                    elif isinstance(content, Dropdown):
                        safemenu2 = f'{safemenu}{content.name.replace(" ", "")}'
                        with self.divc(f'row {safemenu}', style='display:none'):
                            with self.divc('offset-1 col-10'):
                                self._toggle_button(content.name, safemenu2)
                        for dropdown in content:
                            if isinstance(dropdown, Gosub):
                                with self.divc(f'row {safemenu2}', style='display:none'):
                                    with self.divc('offset-2 col-8'):
                                        self._submenu_link(dropdown.name)
                            elif isinstance(dropdown, Action):
                                with self.divc(f'row {safemenu2}', style='display:none'):
                                    with self.divc('offset-2 col-8'):
                                        self._action_link(submenu, dropdown)
                    elif isinstance(content, Action):
                        with self.divc(f'row {safemenu}', style='display:none'):
                            with self.divc('offset-1 col-10'):
                                self._action_link(submenu, content)
