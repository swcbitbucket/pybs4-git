"""
"""
import os
from cherrypy.lib.auth_digest import H

from pybs4.util.markup import Q
from pybs4.util.files import read, write, split


def new_row(p, ctxt):
    """
    """
    p.h3('New Row')
    FILE = ctxt.FILE
    COLS = split(FILE, ctxt.COLS)
    data = read(FILE)
    row = [ctxt.pop(f'NEW_{col}') for col in COLS]
    # Very special case of a user, realm and password
    if FILE.endswith('.htdigest'):
        row[-1] = H(':'.join(row))
    if row[0]:
        data.append(tuple(row))
        write(FILE, data)
    view(p, ctxt)


def delete_row(p, ctxt):
    """
    """
    ROW = ctxt.consume('ROW')
    if ROW is None:
        view(p, ctxt)
        return
    FILE = ctxt.FILE
    data = read(FILE)
    data.pop(int(ROW))
    write(FILE, data)
    view(p, ctxt)


def save_row(p, ctxt):
    """
    """
    ROW = ctxt.consume('ROW')

    FILE = ctxt.FILE
    COLS = split(FILE, ctxt.COLS)

    if ctxt.isflag('CANCEL') or ROW is None:
        ctxt.consume_all('NEW_')
        view(p, ctxt)
        return

    assert ctxt.isflag('SAVE')
    data = read(FILE)
    data.pop(int(ROW))
    row = [ctxt.pop(f'NEW_{col}') for col in COLS]
    # Very special case of a user, realm and password
    if FILE.endswith('.htdigest'):
        row[-1] = H(':'.join(row))

    data.append(tuple(row))
    write(FILE, data)
    view(p, ctxt)


def edit_row(p, ctxt):
    """
    """
    ROW = ctxt.consume('ROW')
    if ROW is None:
        view(p, ctxt)
        return

    FILE = ctxt.FILE
    COLS = split(FILE, ctxt.COLS)
    p.h3(f'Edit {FILE} Row {ROW}')

    data = read(FILE)
    row = data[int(ROW)]

    with p.form(class_='form-inline', action='/action', method='post'):
        p.ctxt_hidden(CALL='save_row', ROW=ROW)
        for col in COLS:
            if 'password' in col.lower():
                p.input(type_='password', class_='form-control', name=f'NEW_{col}', autocomplete=False, placeholder=col)
            else:
                p.input(type_='text', class_='form-control', name=f'NEW_{col}', value=getattr(row, col), autocomplete=True, required=True)
        p.button('Save', type_='submit', class_='btn btn-primary', name='SAVE')
        p.button('Cancel', type_='submit', class_='btn btn-primary', name='CANCEL')


def view(p, ctxt):
    """
    """
    if not (ctxt.FILE and ctxt.COLS):
        p.h3('Something went wrong - start again')
        return

    p.h3(f'View {ctxt.FILE}')

    FILE = ctxt.FILE
    COLS = split(FILE, ctxt.COLS)

    if not os.path.exists(FILE):
        write(FILE, cols=COLS)

    with p.form(class_='form-inline', action='/action', method='post'):
        p.ctxt_hidden(CALL='new_row')
        for col in COLS:
            p.input(type_='text', class_='form-control', placeholder=f'New {col}', name=f'NEW_{col}')
        p.button('Create', type_='submit', class_='btn btn-primary')
    p.hr()

    data = read(ctxt.FILE)
    with p.divc('col-3'):
        with p.tablec():
            with p.thead():
                p.tr(Q.th(COLS + ['Delete']))
            with p.tbody():
                for idx, row in enumerate(data):
                    with p.tr():
                        with p.td():
                            p.ctxt_link(getattr(row, COLS[0]), CALL='edit_row', ROW=idx)
                        for col in COLS[1:]:
                            with p.td():
                                value = getattr(row, col)
                                if 'password' in col.lower():
                                    p += '*' * 10
                                else:
                                    p += value
                        with p.td():
                            p.ctxt_link(p.icon('minus-circle', 'Delete'), CALL='delete_row', ROW=idx)


default = view
