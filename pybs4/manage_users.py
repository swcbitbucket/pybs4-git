"""
The idea here is to add a new user to file with the format MD5(user,realm,password)
for password '4x5istwelve'
eg alice:wonderland:3238cdfe91a8b2ed8e39646921a02d4c
"""

import os
from random import sample

from cherrypy.lib.auth_digest import H

from pybs4.util.logging import error

from pybs4.util import files

PWFILE = 'users.htdigest'


def email(user, realm, password):
    """
    """
    error(f'email NYI {locals()}')


def manage(user, realm, password):
    """
    """
    USER = user.lower()
    REALM = realm.lower()
    if password:
        PASSWORD = password
    else:
        PASSWORD = ''.join(sample('1234567890abcdefghijklmnopqrstuvwxyz', 8))

    KEY = USER, REALM

    if not os.path.exists(PWFILE):
        files.write(PWFILE, cols='User:Realm:Password')

    db = {}
    for row in files.yield_namedtuple(PWFILE):
        key = row.User, row.Realm
        if key in db:
            error(f'deleting duplicate user {KEY}')
            continue
        db[key] = row.Password

    if KEY in db:
        print(f'{KEY} is already a user - update password')
    db[KEY] = H(':'.join((USER, REALM, PASSWORD)))

    rows = []
    for (u, r), p in db.items():
        rows.append((u, r, p))
    files.write(PWFILE, rows)

    email(USER, REALM, PASSWORD)


if __name__ == '__main__':
    """
    """

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('user', help='eg steve@work.com - will be converted to lower case')
    parser.add_argument('realm', help='eg wonderland - will be converted to lower case')
    parser.add_argument('password', nargs='?', default=None, help='If blank will be auto generated and posted by email')
    args = parser.parse_args()
    manage(args.user, args.realm, args.password)
